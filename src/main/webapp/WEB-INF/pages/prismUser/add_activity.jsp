<%--
  Created by IntelliJ IDEA.
  User: kenwambua
  Date: 3/9/2015
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Kenya Prison Service</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
  <!-- Core CSS - Include with every page -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

  <!-- Page-Level Plugin CSS - Dashboard -->
  <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/plugins/timeline/timeline.css" rel="stylesheet">
  <!-- Core Scripts - Include with every page -->
  <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

  <!-- SB Admin CSS - Include with every page -->
  <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
  <c:set var="contextPath" value="${pageContext.request.contextPath}" />
  <c:url value="j_spring_security_logout" var="logout" />
  <c:url var="addAction" value="/prism_User/add_activity/save"/>

  <script type="text/javascript">
    window.setTimeout(function () {
      $("#addActivity_error_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#addActivity_message_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
</head>

<body>

<div id="wrapper">

  <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${contextPath}/prism_User">Prison Prison Service</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

      <!-- /.dropdown -->
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user fa-fw"></i>  <span>${prismUserName}  <i class="caret"></i></span></a>
        <ul class="dropdown-menu dropdown-user">
          <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
          </li>
          <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
          </li>
          <li class="divider"></li>
          <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
          </li>
        </ul>
        <!-- /.dropdown-user -->
      </li>
      <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="${contextPath}/prism_User"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
          </li>
          <li>
            <a href="${contextPath}/prism_User/register_inmate"><i class="fa fa-edit fa-fw"></i>Register Inmates</a>
          </li>
          <li>
            <a href="${contextPath}/prism_User/add_Visitors"><i class="fa fa-pencil-square fa-fw"></i>Register Visitors</a>
          </li>
          <li>
            <a href="${contextPath}/prism_User/view_Inmates"><i class="fa fa-envelope-o fa-fw"></i>Inmates Records</a>
          </li>
          <li>
            <a href="${contextPath}/prism_User/view_Visitors"><i class="fa fa-folder-open fa-fw"></i>Visitors Records</a>
          </li>
          <%--<li>--%>
          <%--<a href=""><i class="fa fa-briefcase fa-fw"></i>Post Parole</a>--%>
          <%--</li>--%>
          <li>
            <a href="${contextPath}/prism_User/add_Activity"><i class="fa fa-briefcase fa-fw"></i>Inmate Activities</a>
          </li>
        </ul>
        <!-- /#side-menu -->
      </div>
      <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
  </nav>

  <div id="page-wrapper">
    <!--<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">User Role Management</h1>
		</div>
		 /.col-lg-12
	</div>-->

    <!-- /.row -->
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Inmate Activities</h1>
      </div>
      <form  method="post" action="${addAction}" name="activity" command="add_activity" class="register">
        <br>
        <div class="row-fluid">
          <c:if test="${not empty error}">
            <div class="alert alert-danger" id="addActivity_error_alert"><i
                    class="fa fa-ban"></i> ${error}</div>
          </c:if>
          <c:if test="${not empty message}">
            <div class="alert alert-success" id="addActivity_message_alert"><i
                    class="fa fa-check"></i> ${message}</div>
          </c:if>
          <div class="col-sm-12">
            <fieldset>
              <legend>Details </legend>
              <p>
                <label>Prisoner ID </label>
                <input  type="text"  class="form-control" name="inmate_id" id="inmate_id" placeholder="Prisoner Id" size="100px">
              </p>
              <p>
                <label>Activity Description </label>
                <textarea rows="4" cols="50" class="form-control" name="comment" id="comment" placeholder="Prisoner activities"></textarea>
              </p>
              <p>

                <input type="hidden" value="${prismUserName}" class="form-control" name="warden_id" id="warden_id" placeholder="Prisoner activities">
              </p>

              <legend>Nature of Activity </legend>

              <p> <label>Postitive</label>
                <input type="radio" class="form-control" name="cartegory" id="cartegory1" value="positive">
              </p>
              <p> <label>Negative </label>
                <input type="radio" class="form-control" name="cartegory" id="cartegory2" value="negative">
              </p>


            </fieldset>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3 col-md-offset-5">

            <button class="button" type="submit">Save <span class="glyphicon glyphicon-folder-open"></span></button>
          </div>
        </div>

      </form>

    </div>


  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<!-- Page-Level Plugin Scripts - Dashboard -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

</body>

</html>
