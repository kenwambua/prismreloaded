<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Kenya Service Prison</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <!-- Core CSS - Include with every page -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
    <%--<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.0.js"></script>--%>
    <link href="${pageContext.request.contextPath}/resources/css/daterangepicker/jquery-ui.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <c:url value="j_spring_security_logout" var="logout" />
    <c:url var="addAction" value="/prism_User/register_inmates/save"/>
    <script>
        $(function() {
            $( "#dob" ).datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: "-100Y -1M -10D",
                maxDate:0
            });
        });
        $(function() {
            $('#release_date').datepicker({
                dateFormat: "dd-M-yy"
            });
            $( "#sentence_start_date" ).datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: "-100Y -1M -10D",
                maxDate:0,
                onSelect: function(date){

                    var y=parseInt(document.getElementById("duration1").value);
                    var date2 = $('#sentence_start_date').datepicker('getDate');
                    date2.setDate(date2.getDate()+ y);
                    $('#release_date').datepicker('setDate', date2);
                }
            });
        });
    </script>


    <script type="text/javascript">
        window.setTimeout(function () {
            $("#addGroup_error_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>
    <script type="text/javascript">
        window.setTimeout(function () {
            $("#addGroup_message_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>
    <%--<script>--%>
        <%--$(function() {--%>
            <%--$(".firstcal").datepicker({--%>
                <%--dateFormat: "dd/mm/yy",--%>
                <%--onSelect: function(dateText, instance) {--%>
                    <%--var x = document.getElementById("duration1").value;--%>
                    <%--var q = parseInt(x);--%>
                    <%--date = $.datepicker.parseDate(instance.settings.dateFormat, dateText, instance.settings);--%>
                    <%--date.setMonth(date.getMonth() + q);--%>
                    <%--$(".secondcal").datepicker("setDate", date);--%>
                <%--}--%>
            <%--});--%>
            <%--$(".secondcal").datepicker({--%>
                <%--dateFormat: "dd/mm/yy"--%>
            <%--});--%>
        <%--});--%>
    <%--</script>--%>
</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/prism_User">Prison Prison Service</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <span>${prismUserName}  <i class="caret"></i></span></a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${contextPath}/prism_User"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/register_inmate"><i class="fa fa-edit fa-fw"></i>Register Inmates</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/add_Visitors"><i class="fa fa-pencil-square fa-fw"></i>Register Visitors</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/view_Inmates"><i class="fa fa-envelope-o fa-fw"></i>Inmates Records</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/view_Visitors"><i class="fa fa-folder-open fa-fw"></i>Visitors Records</a>
                    </li>
                    <%--<li>--%>
                    <%--<a href=""><i class="fa fa-briefcase fa-fw"></i>Post Parole</a>--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prism_User/add_Activity"><i class="fa fa-briefcase fa-fw"></i>Inmate Activities</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <h1>Inmates Registration</h1>
        <div class="col-sm-12">
            <div class="alert alert-info">
                <p>Fields marked with <span style="color:red"> * </span>are required</p>
            </div>
        </div>
        <form method="post" action="${addAction}" commandName="inmate_records" name="user" class="register">
            <br>
            <c:if test="${not empty error}">
                <div class="alert alert-danger" id="addGroup_error_alert"><i
                        class="fa fa-ban"></i> ${error}</div>
            </c:if>
            <c:if test="${not empty message}">
                <div class="alert alert-success" id="addGroup_message_alert"><i
                        class="fa fa-check"></i> ${message}</div>
            </c:if>
            <div class="row">
                <div class="col-sm-12">
                    <fieldset>
                        <legend> ID NUMBERS </legend>
                        <p>
                            <label>Prisoner Number * </label>
                            <input type="text"  name="prisonerId" id="prisonerId" required pattern="^[0-9]+$" placeholder="Prisoner's Prison Number"/>
                        </p>
                        <p>
                            <label>National ID Number * </label>
                            <input type="text" name="id_number" id="id_number" placeholder="Prisoner's ID Number" required pattern="^[0-9]+$"/>
                        </p>

                    </fieldset>
                    <%--<fieldset>--%>
                    <%--<legend>Upload  image </legend>--%>
                    <%--<div class="col-sm-6">--%>
                    <%----%>
                    <%--<div><img id="blah" src="${pageContext.request.contextPath}/resources/img/profile.jpg" alt="select image" /></div>--%>
                    <%--<div><input type='file' onchange="readURL(this);" /></div>--%>
                    <%----%>
                    <%--</div>--%>
                    <%--</fieldset>--%>
                </div>
            </div>
            <div class="row">
                <fieldset>
                    <legend>Names </legend>
                    <p>
                        <label>Surname Name* </label>
                        <input type="text" name="surname" id="surname" <%--required pattern="[A-Za-z']*"--%>/>
                    </p>
                    <p>
                        <label>First Name * </label>
                        <input type="text" name="firstname" id="firstname" <%--required pattern="[A-Za-z']*"--%>/>
                    </p>
                    <p>
                        <label>Other (Names) * </label>
                        <input type="text" name="other_name" id="other_name" <%--required pattern="[A-Za-z']*"--%>/>
                    </p>
                    <legend>Further Information </legend>

                    <p>
                        <label>Birthdate * </label>
                        <input type="text" name="dob" id="dob"  placeholder="dd/mm/yyyy" required*/>
                    </p>
                    <p>
                        <label>Weight* </label>
                        <input type="text" name="weight" id="weight" placeholder="Enter in Kgs" required<%--required pattern="[A-Za-z']*"--%>/>
                    </p>
                    <p>
                        <label>Height * </label>
                        <input type="text" name="height" id="height" placeholder="Enter in Feet" required<%--required pattern="[A-Za-z']*"--%>/>
                    </p>

                    <p>
                        <label>Complexion * </label>
                        <select name="complexion" id="complexion">
                            <option value="Black">Black </option>
                            <option value="White">White </option>
                            <option value="Asian">Asian </option>
                            <option value="Australoid">Australoid </option>
                        </select>
                    </p>


                    <legend>Demographics </legend><!-- Contacts -->
                    <%--<p>--%>
                        <%--<label>Town * </label>--%>
                        <%--<input type="text" name="town" id="town" &lt;%&ndash;required pattern="[A-Za-z]*"&ndash;%&gt;/>--%>
                    <%--</p>--%>
                    <p>
                        <label>County * </label>
                        <select name="county" id="county">

                            <option value="mombasa">Mombasa</option>
                            <option value="kwale">Kwale</option>
                            <option value="kilifi">Kilifi</option>
                            <option value="tana river">Tana River</option>
                            <option value="lamu">Lamu</option>
                            <option value="taita taveta">Taita Taveta</option>
                            <option value="garissa">Garissa</option>
                            <option value="wajir">Wajir</option>
                            <option value="mandera">Mandera</option>
                            <option value="marsabit">Marsabit</option>
                            <option value="isiolo">Isiolo</option>
                            <option value="meru">Meru</option>
                            <option value="tharaka nithi">Tharaka Nithi</option>
                            <option value="embu">Embu</option>
                            <option value="kitui">Kitui</option>
                            <option value="machakos">Machakos</option>
                            <option value="makueni">Makueni</option>
                            <option value="nyandarua">Nyandarua</option>
                            <option value="nyeri">Nyeri</option>
                            <option value="kirinyaga">Kirinyaga</option>
                            <option value="murang'a">Murang'a</option>
                            <option value="kiambu">Kiambu</option>
                            <option value="turkana">Turkana</option>
                            <option value="west pokot">West Pokot</option>
                            <option value="samburu">Samburu</option>
                            <option value="trans nzoia">Trans Nzoia</option>
                            <option value="uasin gishu">Uasin Gishu</option>
                            <option value="elgeyo marakwet">Elgeyo Marakwet</option>
                            <option value="nandi">Nandi</option>
                            <option value="baringo">Baringo</option>
                            <option value="laikipia">Laikipia</option>
                            <option value="nakuru">Nakuru</option>
                            <option value="narok">Narok</option>
                            <option value="kajiado">Kajiado</option>
                            <option value="kericho">Kericho</option>
                            <option value="bomet">Bomet</option>
                            <option value="kakamega">Kakamega</option>
                            <option value="vihiga">Vihiga</option>
                            <option value="bungoma">Bungoma</option>
                            <option value="busia">Busia</option>
                            <option value="siaya">Siaya</option>
                            <option value="kisumu">Kisumu</option>
                            <option value="homa bay">Homa Bay</option>
                            <option value="migori">Migori</option>
                            <option value="kisii">Kisii</option>
                            <option value="nyamira">Nyamira</option>
                            <option value="nairobi">Nairobi</option>
                        </select>
                    </p>
                    <%--<p>--%>
                        <%--<label class="optional">District </label>--%>
                        <%--<input type="text" name="district" id="district"/>--%>
                    <%--</p>--%>
                    <p>
                        <label> Location </label>
                        <input type="text" name="location" id="location" placeholder="location" required/>
                    </p>

                </fieldset>

                <fieldset>

                    <legend>Next of Kins Details </legend><!-- Next of Kin -->
                    <p>
                        <label>ID Number * </label>
                        <input type="text" name="kin_id" id="kin_id" placeholder="Enter Id Number" <%--required pattern="[0]{1}[7]{1}[0-3]{1}[0-9]{7}"--%>/>
                    </p>
                    <p>
                        <label>Surname*
                        </label>
                        <input type="text" name="kin_surname" id="kin_surname"  placeholder="Name of nearest relative/friend" required/>
                    </p>
                    <p>
                        <label>Other Names*
                        </label>
                        <input type="text" name="kin_other_names" id="kin_other_names"  placeholder="Name of nearest relative/friend" required/>
                    </p>
                    <p>
                        <label>Relationship
                        </label>
                        <select name="relationship" id="relationship">

                            <option value="Spouse">Spouse
                            </option>
                            <option value="Sibling">Sibling
                            </option>
                            <option value="Parent">Parent
                            </option>
                            <option value="Relative">Relative
                            </option>
                            <option value="Friend">Friend
                            </option>
                            <option value="Other">Other
                            </option>
                        </select>
                    </p>
                    <p>
                        <label>Phone * </label>
                        <input type="text" name="kin_phone" id="kin_phone" placeholder="07xx-xxx-xxx" <%--required pattern="[0]{1}[7]{1}[0-3]{1}[0-9]{7}"--%>/>
                    </p>
                    <%--<p>--%>
                    <%--<label class="optional">Email </label>--%>
                    <%--<input placeholder="me@gmail.com" type="text"  <%--required pattern="[a-zA-Z0-9]{1,50}[@]{1}[a-z]{1,20}[.][a-z]{2,10}" oninvalid="setCustomValidity('Email address should be in the patterm me@gmail.com ')"/>--%>
                    <%--</p>--%>
                    <%--<p>--%>
                    <%--<label class="optional">Postal Address </label>--%>
                    <%--<input type="text" name="" id="" pattern="[0-9]{1,4}-[0-9]{5}*"/>--%>
                    <%--</p>--%>
                    <%--<p>--%>
                    <%--<label class="optional">Next of Kin Address--%>
                    <%--</label>--%>
                    <%--<input type="text" name="" id="" pattern="[0-9]{1,4}-[0-9]{5}*" placeholder="Kin's person address"/>--%>
                    <%--</p>--%>
                    <legend>Offence section </legend>
                    <p>
                        <label>Crime Committed*</label>
                        <input type="text" name="crime_committed" id="crime_committed"  placeholder="Crime Commited" required/>
                    </p>

                    <p>
                        <label>Nature of Offence * </label>
                        <select name="offence_nature" id="offence_nature">

                            <option value="Assault">Assault</option>
                            <option value="Arson">Arson</option>
                            <option value="Battery">Battery</option>
                            <option value="Burglary">Burglary</option>
                            <option value="Conspiracy">Conspiracy</option>
                            <option value="Embezzlement">Embezzlement</option>
                            <option value="False Pretense">False Pretense</option>
                            <option value="Forgery">Forgery</option>
                            <option value="Homicide">Homicide</option>
                            <option value="Kidnapping">Kidnapping</option>
                            <option value="Robbery">Robbery</option>
                        </select>
                    </p>

                    <p>
                        <label>Duration*</label>
                        <input type="text" name="duration" id="duration1"  placeholder="Enter in years" required/>
                    </p>
                    <p>
                        <label>Date started Sentence * </label>
                        <input type="text" name="sentence_start_date" id="sentence_start_date" class="firstcal" size="12"  required*/>
                    </p>
                    <p>
                        <label>Release Date </label>
                        <input type="text" name="release_date" id="release_date" size="12"  class="secondcal" required*/>
                    </p>
                    <p>
                        <label>Sentencing Court*</label>
                        <input type="text" name="court" id="court"  placeholder="Sentencing Court" required/>
                    </p>
                    <p>
                        <label >Comments</label>
                        <input type="text" name="comments" id="comments"  placeholder="Comment" required/>
                    </p>
                    <p>
                        <label class="hidden">status</label>
                        <input type="hidden" name="status" id="status"  value="held" required/>
                    </p>

                </fieldset>
            </div>


            <div class="row">
                <div class="col-md-5 col-md-offset-5">
                    <button class="button" type="submit">Register Inmate <span class="glyphicon glyphicon-folder-open"></span></button>
                </div>
            </div>
        </form>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Core Scripts - Include with every page -->
<%--<script>--%>
<%--function readURL(input) {--%>
<%--if (input.files && input.files[0]) {--%>
<%--var reader = new FileReader();--%>

<%--reader.onload = function (e) {--%>
<%--$('#blah')--%>
<%--.attr('src', e.target.result)--%>
<%--.width(140)--%>
<%--.height(150);--%>
<%--};--%>

<%--reader.readAsDataURL(input.files[0]);--%>
<%--}--%>
<%--}--%>


<%--</script>--%>

<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Page-Level Plugin Scripts - Dashboard -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

</body>

</html>
