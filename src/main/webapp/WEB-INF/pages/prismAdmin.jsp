
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html; charset=UTF-8" %>

<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Kenya Prison Service</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <!-- Core CSS - Include with every page -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">

    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
   <c:url value="j_spring_security_logout" var="logout" />
</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/prismAdmin">Kenya Prison Service</a>
        </div>

        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <span>${prismAdminName}  <i class="caret"></i></span>
                    <%--<i class="fa fa-caret-down"></i>--%>
                </a>
                <ul class="dropdown-menu dropdown-user">


                    <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <%--<li class="sidebar-search">--%>
                        <%--<div class="input-group custom-search-form">--%>
                            <%--<input type="text" class="form-control" placeholder="Search...">--%>
                                <%--<span class="input-group-btn">--%>
                                <%--<button class="btn btn-default" type="button">--%>
                                    <%--<i class="fa fa-search"></i>--%>
                                <%--</button>--%>
                            <%--</span>--%>
                        <%--</div>--%>
                        <%--<!-- /input-group -->--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prismAdmin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/add_users"><i class="fa fa-table fa-fw"></i> Register User</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/view_Inmates"><i class="fa fa-dashboard fa-fw"></i>Inmate Records</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/reports"><i class="fa fa-table fa-fw"></i> Reports<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${contextPath}/prismAdmin/view_Inmates_Reports">Inmate</a>
                                <a href="${contextPath}/prismAdmin/redirect_report">Personal</a>
                                <a href="${contextPath}/prismAdmin/view_Assault_Reports">Assault Report</a>
                                <a href="${contextPath}/prismAdmin/view_Arson_Reports">Arson</a>
                                <a href="${contextPath}/prismAdmin/view_Burlgary_Reports">Burlgary</a>
                                <a href="${contextPath}/prismAdmin/view_Homicide_Reports">Homicide</a>


                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/view_Visitor"><i class="fa fa-dashboard fa-fw"></i>Visitor Record</a>
                    </li>
                    <%--<li>--%>
                        <%--<a href=""><i class="fa fa-table fa-fw"></i>Parole Requests</a>--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prismAdmin/manage_users"><i class="fa fa-dashboard fa-fw"></i>Manage Users</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/release_Inmates"><i class="fa fa-table fa-fw"></i>Release Inmate</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome To Admin Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/add_users">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-user"></i> </button></a>
                    <h4 style="text-align:center">Register User</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/view_Inmates">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-search"></i> </button></a>
                    <h4 style="text-align:center">Inmate Records</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/reports">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-cog"></i> </button></a>
                    <h4 style="text-align:center">Reports</h4>
                </div>
            </div>
            <%--<div class="col-lg-3">--%>
                <%--<div class="well well-sm"><!-- <a href="hrecords.html"> -->--%>
                    <%--<button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-file"></i> </button></a>--%>
                    <%--<h4 style="text-align:center">Hospital Records</h4>--%>
                <%--</div>--%>

        <!-- /.row -->
        <!-- /.row -->

            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/view_Visitor">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-folder-open"></i></button></a>
                    <h4 style="text-align:center">Visitor Records</h4>
                </div>


            </div>
            <%--<div class="col-lg-3">--%>
                <%--<div class="well well-sm"><!-- <a href="ICD10coding.html"> -->--%>
                    <%--<button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-link"></i> </button></a>--%>
                    <%--<h4 style="text-align:center">Parole Requests</h4>--%>
                    <%--</div>--%>
            <%--</div>--%>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/manage_users"/><!-- <a href="roles.html"> -->
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-certificate"></i> </button></a>
                    <h4 style="text-align:center">Manage User Roles</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prismAdmin/release_Inmates">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-link"></i> </button></a>
                    <h4 style="text-align:center">Release Inmate</h4>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Core Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Page-Level Plugin Scripts - Dashboard -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

</body>

</html>
