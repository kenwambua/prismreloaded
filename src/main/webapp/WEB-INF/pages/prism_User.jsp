
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Kenya Prison Service</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <!-- Core CSS - Include with every page -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Core Scripts - Include with every page -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <c:url value="j_spring_security_logout" var="logout" />
</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/prism_User">Prison Prison Service</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <span>${prismUserName}  <i class="caret"></i></span></a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="${contextPath}/prism_User"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/register_inmate"><i class="fa fa-edit fa-fw"></i>Register Inmates</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/add_Visitors"><i class="fa fa-pencil-square fa-fw"></i>Register Visitors</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/view_Inmates"><i class="fa fa-envelope-o fa-fw"></i>Inmates Records</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prism_User/view_Visitors"><i class="fa fa-folder-open fa-fw"></i>Visitors Records</a>
                    </li>
                    <%--<li>--%>
                        <%--<a href=""><i class="fa fa-briefcase fa-fw"></i>Post Parole</a>--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prism_User/add_Activity"><i class="fa fa-briefcase fa-fw"></i>Inmate Activities</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome to User DashBoard</h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prism_User/register_inmate">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-user"></i> </button></a>
                    <h4 style="text-align:center">Register Inmates</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prism_User/add_Visitors">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-pencil"></i> </button></a>
                    <h4 style="text-align:center">Register Visitors</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prism_User/view_Inmates">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-search"></i> </button></a>
                    <h4 style="text-align:center">Inmate records</h4>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prism_User/view_Visitors">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-folder-open"></i> </button></a>
                    <h4 style="text-align:center">Visitors Records</h4>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div class="row">
            <%--<div class="col-lg-3">--%>
                <%--<div class="well well-sm"><a href="">--%>
                    <%--<button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-envelope"></i> </button></a>--%>
                    <%--<h4 style="text-align:center">Post Parole</h4>--%>
                <%--</div>--%>
            <%--</div>--%>
            <div class="col-lg-3">
                <div class="well well-sm"><a href="${contextPath}/prism_User/add_Activity">
                    <button type="button" style="margin-left:60px;margin-top:10px;" class="btn btn-primary btn-circle btn-xl"><i class="fa fa-pencil-square-o"></i> </button></a>
                    <h4 style="text-align:center">Record Inmate Activities</h4>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



<!-- Page-Level Plugin Scripts - Dashboard -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

</body>

</html>
