
<!DOCTYPE>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>System Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style3.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/animate-custom.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <script type="text/javascript">
        window.setTimeout(function () {
            $("#error_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>
    <script type="text/javascript">
        window.setTimeout(function () {
            $("#message_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>
</head>
<body>
<div class="container-fluid">
    <header>
        <div class="page-header">
            <div class="row-fluid">
                <div class="col-md-1">
                    <img src="${pageContext.request.contextPath}/resources/img/kenya.jpg">
                </div>
                <div class="col-md-1" style="padding-top:20px;">

                </div>
                <div class="col-md-4">
                    <div class="row"> <h3 class="logo">KENYA <small > PRISON SERVICE </small></h3> </div>
                </div>

            </div>

        </div>
    </header>

    <div class="container">
        <div id="container_demo" >
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form">

                    <form  method="post" action="<c:url value='j_spring_security_check' />" name="loginForm" class="login_reg">
                        <br>
                        <c:if test="${not empty error}">
                            <div class="alert alert-danger" id="error_alert"><i class="fa fa-ban"></i> ${error}</div>
                        </c:if>
                        <c:if test="${not empty message}">
                            <div class="alert alert-success" id="message_alert"><i class="fa fa-check"></i> ${message}</div>
                        </c:if>
                        <h1>Log in</h1>
                        <div class="row">
                            <div class="alert alert-info">
                                <p>Fields marked with <span style="color:red"> * </span>are required</p>
                            </div>
                        </div>
                        <div class="row">
                            <p>
                                <label for="username"> Username * </label>
                                <input id="username" name="staffUsername" required="required" type="text" placeholder="johndoe"
                                       <%--pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"--%>
                                       title="Enter correct Username"/>
                            </p>
                            <p>
                                <label for="password" class="youpasswd" data-icon="p"> Your password *</label>
                                <input id="password" name="staffPassword" type="password" required="" placeholder="*******"
                                       <%--pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"--%>
                                       title="Password Must ContainUpperCase, LowerCase, Number/SpecialChar and min  4 Characters"/>
                            </p>
                        </div>
                        <div class="row">
                            <%--<p class="keeplogin">--%>
                                <%--<label for="loginkeeping">Keep me logged in</label>--%>
                                <%--<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" />--%>
                            <%--</p>--%>
                        </div>
                        <div class="row">
                            <p class="login button">
                                <Button class="submit signin_button" type="submit" name="Login">Login</Button>

                            </p>
                        </div>
                        <div class="row">
                          <b>Admin</b> Username=12345 password=demo1234 </br>
                           <b>User</b> Username=54321 password=1234demo
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>

<script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
</body>
</html>





