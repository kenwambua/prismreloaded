<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Kenya Prison Service</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <!-- Core CSS - Include with every page -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/plugins/timeline/timeline.css" rel="stylesheet">
    <!-- Core Scripts - Include with every page -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- SB Admin CSS - Include with every page -->
    <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <c:url value="j_spring_security_logout" var="logout" />
    <c:url var="addAction" value="/prismAdmin/add_users/save"/>

    <script type="text/javascript">
        window.setTimeout(function () {
            $("#addGroup_error_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>
    <script type="text/javascript">
        window.setTimeout(function () {
            $("#addGroup_message_alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 2500);
    </script>

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${contextPath}/prismAdmin">Kenya Prison Service</a>
            </div>

            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <span>${prismAdminName}  <i class="caret"></i></span>
                        <%--<i class="fa fa-caret-down"></i>--%>
                    </a>
                    <ul class="dropdown-menu dropdown-user">


                        <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <%--<li class="sidebar-search">--%>
                        <%--<div class="input-group custom-search-form">--%>
                        <%--<input type="text" class="form-control" placeholder="Search...">--%>
                        <%--<span class="input-group-btn">--%>
                        <%--<button class="btn btn-default" type="button">--%>
                        <%--<i class="fa fa-search"></i>--%>
                        <%--</button>--%>
                        <%--</span>--%>
                        <%--</div>--%>
                        <%--<!-- /input-group -->--%>
                        <%--</li>--%>
                        <li>
                            <a href="${contextPath}/prismAdmin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="${contextPath}/prismAdmin/add_users"><i class="fa fa-table fa-fw"></i> Register User</a>
                        </li>
                        <li>
                            <a href="${contextPath}/prismAdmin/view_Inmates"><i class="fa fa-dashboard fa-fw"></i>Inmate Records</a>
                        </li>
                        <li>
                            <a href="${contextPath}/prismAdmin/reports"><i class="fa fa-table fa-fw"></i> Reports<span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="${contextPath}/prismAdmin/view_Inmates_Reports">Inmate</a>
                                    <a href="${contextPath}/prismAdmin/redirect_report">Personal</a>
                                    <a href="${contextPath}/prismAdmin/view_Assault_Reports">Assault Report</a>
                                    <a href="${contextPath}/prismAdmin/view_Arson_Reports">Arson</a>
                                    <a href="${contextPath}/prismAdmin/view_Burlgary_Reports">Burlgary</a>
                                    <a href="${contextPath}/prismAdmin/view_Homicide_Reports">Homicide</a>


                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="${contextPath}/prismAdmin/view_Visitor"><i class="fa fa-dashboard fa-fw"></i>Visitor Record</a>
                        </li>
                        <%--<li>--%>
                        <%--<a href=""><i class="fa fa-table fa-fw"></i>Parole Requests</a>--%>
                        <%--</li>--%>
                        <li>
                            <a href="${contextPath}/prismAdmin/manage_users"><i class="fa fa-dashboard fa-fw"></i>Manage Users</a>
                        </li>
                        <li>
                            <a href="${contextPath}/prismAdmin/release_Inmates"><i class="fa fa-table fa-fw"></i>Release Inmate</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <!--<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Role Management</h1>
                </div>
                 /.col-lg-12 
            </div>-->
            
            <!-- /.row -->
            <div class="row">
            <div class="col-lg-12">
                    <h1 class="page-header">Add New User</h1>
                </div>
                    <form method="post" action="${addAction}" name="user" class="register">
                        </br>
                        <c:if test="${not empty error}">
                            <div class="alert alert-danger" id="addGroup_error_alert"><i
                                    class="fa fa-ban"></i> ${error}</div>
                        </c:if>
                        <c:if test="${not empty message}">
                            <div class="alert alert-success" id="addGroup_message_alert"><i
                                    class="fa fa-check"></i> ${message}</div>
                        </c:if>
                    <div class="row-fluid">
                        <div class="col-sm-12">
                             <fieldset>
                                <legend>Details </legend>
                                <p>
                                  <label>Warden Number</label>
                                  <input type="text"  name="warden_Number" id="warden_Number" class="form-control" placeholder="Warden Number">
                                </p>
                                 <p>
                                     <label>id Number </label>
                                     <input type="text"  name="id_number" id="id_number" class="form-control" placeholder="id number">
                                 </p>
                                <p>
                                    <label>Surname </label>
                                    <input type="text"  name="surname" id="surname" class="form-control" placeholder="Doe">
                                </p>
                                <p>
                                  <label>Other Names </label>
                                  <input type="text"  name="other_Names" id="other_Names" class="form-control" placeholder="John Smith">
                                </p>
                                <p>
                                    <label>PhoneNumber</label>
                                    <input type="text"  name="phone_Number" id="phone_Number" class="form-control" placeholder="0722000000">
                                </p>
                                 <p>
                                     <label>User Role</label>
                                     <select name="ROLE" id="user_Role">
                                         <option value="ROLE_PRISMUSER">USER</option>
                                         <option value="ROLE_PRISMADMIN">ADMIN</option>

                                     </select>
                                 </p>
                                <p>
                                  <label>Password </label>
                                  <input type="password" name="password" id="password" class="form-control" placeholder="*********">
                                </p>
                               
                            </fieldset>
                            <fieldset>

                                <legend>Next of Kins Details </legend><!-- Next of Kin -->
                                <p>
                                    <label>ID Number * </label>
                                    <input type="text" name="kin_Id" id="kin_Id"  class="form-control" placeholder="Id Number" required/>
                                </p>
                                <p>
                                    <label>Surname*</label>
                                    <input type="text"  name="kin_Surname" id="kin_Surname"  class="form-control" placeholder="Kin's Surname" required/>
                                </p>
                                <p>
                                    <label>Other Names*</label>
                                    <input type="text"  name="kin_Other_Name" id="kin_Other_Name" class="form-control" placeholder="Kin's other names" required/>
                                </p>
                                <p>
                                <label>Kin's Phone number*</label>
                                <input type="text"  name="kin_phone" id="kin_phone"  class="form-control" placeholder="0722000000" required/>
                                </p>
                                
                                <p>
                                    <label>Relationship</label>
                                    <select name="kin_Relationship" id="kin_Relationship">

                                        <option value="Spouse">Spouse</option>
                                        <option value="Sibling">Sibling</option>
                                        <option value="Parent">Parent</option>
                                        <option value="Relative">Relative</option>
                                        <option value="Friend">Friend</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </p>
                                <p hidden="hidden">
                                    <label>Status</label>
                                    <input type="hidden"  name="status" id="status" value="1"  class="form-control" placeholder="0722000000" />
                                </p>

                            </fieldset>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-md-offset-5">
                            <button class="button" type="submit">Save Changes <span class="glyphicon glyphicon-folder-open"></span></button>
                        </div>
                    </div>

                </form>
                
            </div>
            
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

</body>

</html>
