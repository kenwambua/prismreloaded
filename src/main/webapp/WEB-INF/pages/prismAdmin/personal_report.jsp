<%--
  Created by IntelliJ IDEA.
  User: kenwambua
  Date: 2/28/2015
  Time: 5:49 PM
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.sql.*" %>

<%ResultSet resultset2 = null;%>
<%ResultSet rs2 = null;%>
<%ResultSet rs3 = null;%>

<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Kenya Service Prison</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
  <!-- Core CSS - Include with every page -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

  <!-- Page-Level Plugin CSS - Dashboard -->
  <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

  <!-- SB Admin CSS - Include with every page -->
  <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
  <!-- Core Scripts - Include with every page -->
  <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

  <!-- Page-Level Plugin Scripts - Dashboard -->
  <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

  <!-- Page-Level Plugin Scripts - Tables -->
  <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/jquery.dataTables.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/dataTables.bootstrap.js"></script>
  <c:set var="contextPath" value="${pageContext.request.contextPath}" />
  <c:url value="j_spring_security_logout" var="logout" />
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_error_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script>
    document.getElementById("demo").innerHTML = Date();
  </script>
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_message_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".viewdata").click(function () { // Click to only happen on announce links
        $("#prisonerId").val($(this).data('id'));
        $("#id_number").val($(this).data('idno'));
        $("#surname").val($(this).data('surname'));
        $("#firstname").val($(this).data('firstname'));
        $("#other_name").val($(this).data('othername'));
        $("#dob").val($(this).data('dob'));
        $("#weight").val($(this).data('weight'));
        $("#height").val($(this).data('height'));
        $("#complexion").val($(this).data('complexion'));
        //        $("#town").val($(this).data('town'));
        $("#county").val($(this).data('county'));
        //        $("#district").val($(this).data('district'));
        $("#location").val($(this).data('location'));
        $("#kin_id").val($(this).data('kin_id'));
        $("#kin_surname").val($(this).data('kin_surname'));
        $("#kin_other_names").val($(this).data('kin_other_names'));
        $("#relationship").val($(this).data('relationship'));
        $("#kin_phone").val($(this).data('kin_phone'));
        $("#crime_committed").val($(this).data('crime_committed'));
        $("#offence_nature").val($(this).data('offence_nature'));
        $("#duration").val($(this).data('duration'));
        $("#sentence_start_date").val($(this).data('sentence_start_date'));
        $("#court").val($(this).data('court'));
        $("#comments").val($(this).data('comments'))
        $('#dataViewModal').modal('show');
      });
    });
  </script>
</head>

<body>
<%
  try {
    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/prism?user=root&password=root");
    Statement statement2 = connection.createStatement();
    resultset2 = statement2.executeQuery("select prisonerId, surname,firstname,crime_committed,offence_nature,duration,"
            +"sentence_start_date, release_date,status from inmate_records WHERE prisonerId='454545' ");
      Statement st2 = connection.createStatement();
        rs2 = st2.executeQuery("select id_number,surname,other_names,phone_number,date_of_visit from visitors WHERE inmate_visited='454545' ");
    Statement st3 = connection.createStatement();
    rs3 = st3.executeQuery("select comment,cartegory,activity_date from inmate_activities WHERE inmate_id='454545' ");


%>

<div id="wrapper">

  <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${contextPath}/prismAdmin">Kenya Prison Service</a>
    </div>

    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user fa-fw"></i> <span>${prismAdminName}  <i class="caret"></i></span>
          <%--<i class="fa fa-caret-down"></i>--%>
        </a>
        <ul class="dropdown-menu dropdown-user">


          <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
          </li>
        </ul>
        <!-- /.dropdown-user -->
      </li>
      <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <%--<li class="sidebar-search">--%>
          <%--<div class="input-group custom-search-form">--%>
          <%--<input type="text" class="form-control" placeholder="Search...">--%>
          <%--<span class="input-group-btn">--%>
          <%--<button class="btn btn-default" type="button">--%>
          <%--<i class="fa fa-search"></i>--%>
          <%--</button>--%>
          <%--</span>--%>
          <%--</div>--%>
          <%--<!-- /input-group -->--%>
          <%--</li>--%>
          <li>
            <a href="${contextPath}/prismAdmin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/add_users"><i class="fa fa-table fa-fw"></i> Register User</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/view_Inmates"><i class="fa fa-dashboard fa-fw"></i>Inmate Records</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/reports"><i class="fa fa-table fa-fw"></i> Reports<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level">
              <li>
                <a href="${contextPath}/prismAdmin/view_Inmates_Reports">Inmate</a>
                <a href="${contextPath}/prismAdmin/redirect_report">Personal</a>
                <a href="${contextPath}/prismAdmin/view_Assault_Reports">Assault Report</a>
                <a href="${contextPath}/prismAdmin/view_Arson_Reports">Arson</a>
                <a href="${contextPath}/prismAdmin/view_Burlgary_Reports">Burlgary</a>
                <a href="${contextPath}/prismAdmin/view_Homicide_Reports">Homicide</a>


              </li>

            </ul>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/view_Visitor"><i class="fa fa-dashboard fa-fw"></i>Visitor Record</a>
          </li>
          <%--<li>--%>
          <%--<a href=""><i class="fa fa-table fa-fw"></i>Parole Requests</a>--%>
          <%--</li>--%>
          <li>
            <a href="${contextPath}/prismAdmin/manage_users"><i class="fa fa-dashboard fa-fw"></i>Manage Users</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/release_Inmates"><i class="fa fa-table fa-fw"></i>Release Inmate</a>
          </li>
        </ul>
        <!-- /#side-menu -->
      </div>
      <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
  </nav>

  <div id="page-wrapper">
    <div class="row">
      <a href="javascript:window.print();" class="btn btn-primary btn-rounded" aria-hidden="true"
         data-dismiss="modal"><i class="ion ion-ios7-trash"></i>Print Report</a>
      <div class="col-lg-12">
        <div class="col-md-1">
          <img src="${pageContext.request.contextPath}/resources/img/header.png">

          <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

          <div class="col-sm-12">

            <div class="panel-heading">Inmate Records</div>

            <div class="panel-body">
              <div class="table-responsive">

                <%--<c:if test="${not empty error}">--%>
                  <%--<div class="alert alert-danger" id="viewGroup_error_alert"><i--%>
                          <%--class="fa fa-ban"></i> ${error}</div>--%>
                <%--</c:if>--%>
                <%--<c:if test="${not empty message}">--%>
                  <%--<div class="alert alert-success" id="viewGroup_message_alert"><i--%>
                          <%--class="fa fa-check"></i> ${message}</div>--%>
                <%--</c:if>--%>
                <br/>
                <c:if test="${!empty listInmate}">

                  <table class="table  " >
                    <thead>
                    <tr>
                      <th>Prisoner Id</th>
                      <th>Surname</th>
                      <th>FirstName</th>
                      <th>Crime</th>
                      <th>Nature of Crime</th>
                      <th>Duration</th>
                      <th>Start Date</th>
                      <th>Release Date</th>
                      <th>Status</th>

                        <%--<th>Start Date</th>--%>
                        <%--<th width="50px"></th>--%>
                    </tr>
                    </thead>
                    <tbody>
                      <%--<c:forEach items="${listInmate}" var="inmate">--%>
                    <% while(resultset2.next()) { %>
                    <tr class="gradeA">
                      <td><%= resultset2.getInt("prisonerId")%></td>
                      <td><%= resultset2.getString("surname")%></td>
                      <td><%= resultset2.getString("firstname")%></td>
                      <td><%= resultset2.getString("crime_committed")%></td>
                      <td><%= resultset2.getString("offence_nature")%></td>
                      <td><%= resultset2.getInt("duration")%>years</td>
                      <td><%= resultset2.getString("sentence_start_date")%></td>
                      <td><%= resultset2.getString("release_date")%></td>
                      <td><%= resultset2.getString("status")%></td>

                    </tr>
                    <% } %>

                    </tbody>

                  </table>
                </c:if>

                <h3>Visitors</h3>
                <table class="table table-bordered  " >
                  <thead>
                  <tr>
                    <th>Id Number</th>
                    <th>Surname</th>
                    <th>Other Names</th>
                    <th>Phone Number</th>
                    <th>Date of Visit</th>


                    <%--<th>Start Date</th>--%>
                    <%--<th width="50px"></th>--%>
                  </tr>
                  </thead>
                  <tbody>
                  <%--<c:forEach items="${listInmate}" var="inmate">--%>
                  <% while(rs2.next()) { %>
                  <tr class="gradeA">
                    <td><%= rs2.getString("id_number")%></td>
                    <td><%= rs2.getString("surname")%></td>
                    <td><%= rs2.getString("other_names")%></td>
                    <td><%= rs2.getInt("phone_number")%></td>
                    <td><%= rs2.getString("date_of_visit")%></td>

                  </tr>
                  <% } %>

                  </tbody>

                </table>
                <h3>Activities</h3>
                <table class="table table-bordered  " >
                  <thead>
                  <tr>
                    <th>Activity</th>
                    <th>Nature of Activity</th>
                    <th>Date</th>



                    <%--<th>Start Date</th>--%>
                    <%--<th width="50px"></th>--%>
                  </tr>
                  </thead>
                  <tbody>
                  <%--<c:forEach items="${listInmate}" var="inmate">--%>
                  <% while(rs3.next()) { %>
                  <tr class="gradeA">
                    <td><%= rs3.getString("comment")%></td>
                    <td><%= rs3.getString("cartegory")%></td>
                    <td><%= rs3.getString("activity_date")%></td>

                  </tr>
                  <% } %>

                  </tbody>

                </table>
                <p id="">
                <h4>
                  Printed by: Kennedy Kamau </br>
                  on <p id="demo"></p>
                  <script>
                    document.getElementById("demo").innerHTML = Date();
                  </script>
                  </br>
                  Signature: ..............................
                </h4>
                </p>
              </div>
            </div>
            <div class="panel-footer"> </div>

          </div>
        </div>
        <!-- /.row -->
        <!-- Modal -->
        <%--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
        <%--<div class="modal-dialog">--%>
        <%--<div class="modal-content">--%>
        <%--<div class="modal-header">--%>
        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
        <%--<h4 class="modal-title" id="myModalLabel">Staff Details</h4>--%>
        <%--</div>--%>
        <%--<div class="modal-body">--%>
        <%--<table class="table table-bordered" id="dataTables-example">--%>
        <%--<thead>--%>
        <%--<tr>--%>
        <%--<td>Staff ID</td>--%>
        <%--<td>Staff Names</td>--%>
        <%--<td>Gender</td>--%>
        <%--<td>Staff Role</td>--%>
        <%--<td>Others</td>--%>
        <%--</tr>--%>
        <%--</thead>--%>
        <%--<tbody>--%>
        <%--<tr>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--</tr>--%>
        <%--</tbody>--%>
        <%--</table>--%>
        <%--</div>--%>
        <%--<div class="modal-footer">--%>
        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
        <%--</div>--%>
      </div>
    </div>
  </div>
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  $(document).ready(function() {
    $('#dataTables-example').dataTable();
  });
</script>
<%
  } catch (Exception e) {
    System.out.println("wrong entry" + e);
  }
%>
</body>

</html>
