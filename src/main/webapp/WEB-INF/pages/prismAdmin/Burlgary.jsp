<%--
  Created by IntelliJ IDEA.
  User: kenwambua
  Date: 2/28/2015
  Time: 5:49 PM
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.sql.*" %>

<%ResultSet resultset2 = null;%>
<%ResultSet rs2 = null;%>

<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Kenya Service Prison</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
  <!-- Core CSS - Include with every page -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

  <!-- Page-Level Plugin CSS - Dashboard -->
  <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

  <!-- SB Admin CSS - Include with every page -->
  <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
  <!-- Core Scripts - Include with every page -->
  <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

  <!-- Page-Level Plugin Scripts - Dashboard -->
  <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

  <!-- Page-Level Plugin Scripts - Tables -->
  <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/jquery.dataTables.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/dataTables.bootstrap.js"></script>
  <c:set var="contextPath" value="${pageContext.request.contextPath}" />
  <c:url value="j_spring_security_logout" var="logout" />
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_error_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script>
    document.getElementById("demo").innerHTML = Date();
  </script>
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_message_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".viewdata").click(function () { // Click to only happen on announce links
        $("#prisonerId").val($(this).data('id'));
        $("#id_number").val($(this).data('idno'));
        $("#surname").val($(this).data('surname'));
        $("#firstname").val($(this).data('firstname'));
        $("#other_name").val($(this).data('othername'));
        $("#dob").val($(this).data('dob'));
        $("#weight").val($(this).data('weight'));
        $("#height").val($(this).data('height'));
        $("#complexion").val($(this).data('complexion'));
        //        $("#town").val($(this).data('town'));
        $("#county").val($(this).data('county'));
        //        $("#district").val($(this).data('district'));
        $("#location").val($(this).data('location'));
        $("#kin_id").val($(this).data('kin_id'));
        $("#kin_surname").val($(this).data('kin_surname'));
        $("#kin_other_names").val($(this).data('kin_other_names'));
        $("#relationship").val($(this).data('relationship'));
        $("#kin_phone").val($(this).data('kin_phone'));
        $("#crime_committed").val($(this).data('crime_committed'));
        $("#offence_nature").val($(this).data('offence_nature'));
        $("#duration").val($(this).data('duration'));
        $("#sentence_start_date").val($(this).data('sentence_start_date'));
        $("#court").val($(this).data('court'));
        $("#comments").val($(this).data('comments'))
        $('#dataViewModal').modal('show');
      });
    });
  </script>
</head>

<body>
<%
  try {
    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/prism?user=root&password=root");
    Statement statement2 = connection.createStatement();
    resultset2 = statement2.executeQuery("select prisonerId, surname,firstname,crime_committed,offence_nature,duration,"
            +"sentence_start_date, release_date,status from inmate_records WHERE offence_nature='Burglary' ");
//    Statement st2 = connection.createStatement();
//    rs2 = st2.executeQuery("select Amount , sum(Amount)as total from mice where status = 'Paid' AND ServiceOffered='Incentive'");


%>

<div id="wrapper">

  <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${contextPath}/prismAdmin">Kenya Prison Service</a>
    </div>

    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user fa-fw"></i> <span>${prismAdminName}  <i class="caret"></i></span>
          <%--<i class="fa fa-caret-down"></i>--%>
        </a>
        <ul class="dropdown-menu dropdown-user">


          <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
          </li>
        </ul>
        <!-- /.dropdown-user -->
      </li>
      <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
          <%--<li class="sidebar-search">--%>
          <%--<div class="input-group custom-search-form">--%>
          <%--<input type="text" class="form-control" placeholder="Search...">--%>
          <%--<span class="input-group-btn">--%>
          <%--<button class="btn btn-default" type="button">--%>
          <%--<i class="fa fa-search"></i>--%>
          <%--</button>--%>
          <%--</span>--%>
          <%--</div>--%>
          <%--<!-- /input-group -->--%>
          <%--</li>--%>
          <li>
            <a href="${contextPath}/prismAdmin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/add_users"><i class="fa fa-table fa-fw"></i> Register User</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/view_Inmates"><i class="fa fa-dashboard fa-fw"></i>Inmate Records</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/reports"><i class="fa fa-table fa-fw"></i> Reports<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level">
              <li>
                <a href="${contextPath}/prismAdmin/view_Inmates_Reports">Inmate</a>
                <a href="${contextPath}/prismAdmin/redirect_report">Personal</a>
                <a href="${contextPath}/prismAdmin/view_Assault_Reports">Assault Report</a>
                <a href="${contextPath}/prismAdmin/view_Arson_Reports">Arson</a>
                <a href="${contextPath}/prismAdmin/view_Burlgary_Reports">Burlgary</a>
                <a href="${contextPath}/prismAdmin/view_Homicide_Reports">Homicide</a>


              </li>

            </ul>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/view_Visitor"><i class="fa fa-dashboard fa-fw"></i>Visitor Record</a>
          </li>
          <%--<li>--%>
          <%--<a href=""><i class="fa fa-table fa-fw"></i>Parole Requests</a>--%>
          <%--</li>--%>
          <li>
            <a href="${contextPath}/prismAdmin/manage_users"><i class="fa fa-dashboard fa-fw"></i>Manage Users</a>
          </li>
          <li>
            <a href="${contextPath}/prismAdmin/release_Inmates"><i class="fa fa-table fa-fw"></i>Release Inmate</a>
          </li>
        </ul>
        <!-- /#side-menu -->
      </div>
      <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
  </nav>

  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="col-md-1">
          <img src="${pageContext.request.contextPath}/resources/img/header.png">

          <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
          <a href="javascript:window.print();" class="btn btn-primary btn-rounded" aria-hidden="true"
             data-dismiss="modal"><i class="ion ion-ios7-trash"></i>Print Report</a>
          <div class="col-sm-12">

            <div class="panel-heading">Inmate Records</div>
            <div class="panel-body">
              <div class="table-responsive">
                <%--<c:if test="${not empty error}">--%>
                  <%--<div class="alert alert-danger" id="viewGroup_error_alert"><i--%>
                          <%--class="fa fa-ban"></i> ${error}</div>--%>
                <%--</c:if>--%>
                <%--<c:if test="${not empty message}">--%>
                  <%--<div class="alert alert-success" id="viewGroup_message_alert"><i--%>
                          <%--class="fa fa-check"></i> ${message}</div>--%>
                <%--</c:if>--%>
                <br/>
                <c:if test="${!empty listInmate}">
                  <table class="table  " >
                    <thead>
                    <tr>
                      <th>Prisoner Id</th>
                      <th>Surname</th>
                      <th>FirstName</th>
                      <th>Crime</th>
                      <th>Nature of Crime</th>
                      <th>Duration</th>
                      <th>Start Date</th>
                      <th>Release Date</th>
                      <th>Status</th>
                        <%--<th>Start Date</th>--%>
                        <%--<th width="50px"></th>--%>
                    </tr>
                    </thead>
                    <tbody>
                    <%--<c:forEach items="${listInmate}" var="inmate">--%>
                      <% while(resultset2.next()) { %>
                      <tr class="gradeA">
                        <td><%= resultset2.getInt("prisonerId")%></td>
                        <td><%= resultset2.getString("surname")%></td>
                        <td><%= resultset2.getString("firstname")%></td>
                        <td><%= resultset2.getString("crime_committed")%></td>
                        <td><%= resultset2.getString("offence_nature")%></td>
                        <td><%= resultset2.getInt("duration")%>years</td>
                        <td><%= resultset2.getString("sentence_start_date")%></td>
                        <td><%= resultset2.getString("release_date")%></td>
                        <td><%= resultset2.getString("status")%></td>

                        <%--<td>${inmate.prisonerId}</td>--%>
                        <%--<td>${inmate.surname}</td>--%>
                        <%--<td>${inmate.firstname}</td>--%>
                        <%--<td class="center">${inmate.crime_committed}</td>--%>
                        <%--<td class="center">${inmate.offence_nature}</td>--%>
                        <%--<td>${inmate.duration}</td>--%>
                          <%--&lt;%&ndash;<td>${inmate.kin_phone}</td>&ndash;%&gt;--%>
                        <%--<td>${inmate.sentence_start_date}</td>--%>
                        <%--<td>${inmate.release_date}</td>--%>
                        <%--<td>${inmate.status}</td>--%>

                          <%--<td width="50px">--%>
                          <%--<a href="#"--%>
                          <%--class="btn btn-custom btn-sm viewdata"--%>
                          <%--data-id="${inmate.prisonerId}"--%>
                          <%--data-idno="${inmate.id_number}"--%>
                          <%--data-surname="${inmate.surname}"--%>
                          <%--data-firstname="${inmate.firstname}"--%>
                          <%--data-othername="${inmate.other_name}"--%>
                          <%--data-dob="${inmate.dob}"--%>
                          <%--data-weight="${inmate.weight}"--%>
                          <%--data-height="${inmate.height}"--%>
                          <%--data-complexion="${inmate.complexion}"--%>
                          <%--&lt;%&ndash;town&ndash;%&gt;&ndash;%&gt;--%>
                          <%--data-county="${inmate.county}"--%>
                          <%--&lt;%&ndash;data-district="${inmate.district}"&ndash;%&gt;--%>
                          <%--data-location="${inmate.location}"--%>
                          <%--data-kin_id="${inmate.kin_id}"--%>
                          <%--data-kin_surname="${inmate.kin_surname}"--%>
                          <%--data-kin_other_names="${inmate.kin_other_names}"--%>
                          <%--data-relationship="${inmate.relationship}"--%>
                          <%--data-kin_phone="${inmate.kin_phone}"--%>
                          <%--data-crime_committed="${inmate.crime_committed}"--%>
                          <%--data-offence_nature="${inmate.offence_nature}"--%>
                          <%--data-duration="${inmate.duration}"--%>
                          <%--data-sentence_start_date="${inmate.sentence_start_date}"--%>
                          <%--data-court="${inmate.court}"--%>
                          <%--data-comments="${inmate.comments}">--%>
                          <%--<i class="fa fa-search fa-fw"></i>--%>
                          <%--View More</a></td>--%>
                      </tr>
                    <% } %>
                    <%--</c:forEach>--%>
                    <div class="modal fade" id="dataViewModal" tabindex="-1" role="dialog"
                         aria-labelledby="dataViewModal" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header  bg-blue">
                            <a href="#" class="btn btn-danger btn-sm closebtn" aria-hidden="true"
                               data-dismiss="modal"><i class="ion ion-ios7-trash"></i> Close</a>
                            <h4 class="modal-title center" id="myModalLabel">Inmate Details</h4>
                          </div>
                          <div class="modal-body" id="modalview">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="input-group">
                              <span class="input-group-addon hidden">
                                <i class="fa fa-user"></i></span>
                                  <input type="hidden" name="baptismId" class="form-control"
                                         placeholder="baptism Id" id="baptismId"
                                         value=""/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <div class="input-group">
                              <span class="input-group-addon hidden">
                                <i class="fa fa-user"></i></span>
                                  <input type="hidden" name="baptismUuid" class="form-control"
                                         placeholder="baptism Uuid" id="baptismUuid"
                                         value=""/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <div class="input-group">
							<span class="input-group-addon hidden">
                              <i class="fa fa-calendar"></i></span>
                                  <input type="hidden" name="baptismDateCreated"
                                         class="form-control"
                                         placeholder="baptism Date Created"
                                         id="baptismDateCreated"
                                         value=""/>
                                </div>
                              </div>
                            </div>
                            <br>

                            <div class="row">
                              <div class="col-lg-4">
                                <label>Prisoner Number</label>

                                <div class="input-group">
						  <span class="input-group-addon">
                            <i class="fa fa-table"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Prison Id"
                                         id="prisonerId" name="prisonerId"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>ID Number</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-table"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="ID Number"
                                         id="id_number" name="id_number"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Surname</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Surname"
                                         id="surname" name="surname"
                                         value="" disabled/>
                                </div>
                              </div>
                            </div>
                            <br/>

                            <div class="row">
                              <div class="col-lg-4">
                                <label>First Name</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="First Name"
                                         id="firstname" name="firstname"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Other Name</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Other Name"
                                         id="other_name" name="other_name"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>date of Birth</label>

                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="DOB"
                                         id="dob" name="dob"
                                         value="" disabled/>
                                </div>
                              </div>
                            </div>
                            <br/>

                            <div class="row">
                              <div class="col-lg-4">
                                <label>Weight</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-male"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Weight"
                                         id="weight" name="weight"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Height</label>

                                <div class="input-group">
					<span class="input-group-addon">
                      <i class="fa fa-male"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Height"
                                         id="height" name="height"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Complexion</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-adjust"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Complexion"
                                         id="complexion" name="complexion"
                                         value="" disabled/>
                                </div>
                              </div>
                            </div>
                            <br/>

                            <div class="row">
                              <div class="col-lg-4">
                                <label>County</label>
                                <div class="input-group">
						<span class="input-group-addon">
                            <i class="fa fa-home"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="County"
                                         id="county" name="county"
                                         value="" disabled/>
                                </div>
                              </div>
                                <%--<div class="col-lg-4">--%>
                                <%--<label>Town</label>--%>

                                <%--<div class="input-group">--%>
                                <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-home"></i></span>--%>
                                <%--<input class="form-control" type="text"--%>
                                <%--placeholder="Town"--%>
                                <%--id="town" name="town"--%>
                                <%--value="" disabled/>--%>
                                <%--</div>--%>
                                <%--</div>--%>
                              <div class="col-lg-4">
                                <label>Next of Kin Surname</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Kin Surname"
                                         id="kin_surname" name="kin_surname"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Kin other Name</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Other Names"
                                         id="kin_other_names"
                                         name="kin_other_names"
                                         value="" disabled/>
                                </div>
                              </div>
                            </div>

                            <br/>

                            <div class="row">
                                <%--<div class="col-lg-4">--%>
                                <%--<label>Kin other Name</label>--%>

                                <%--<div class="input-group">--%>
                                <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user"></i></span>--%>
                                <%--<input class="form-control" type="text"--%>
                                <%--placeholder="Other Names"--%>
                                <%--id="kin_other_names"--%>
                                <%--name="kin_other_names"--%>
                                <%--value="" disabled/>--%>
                                <%--</div>--%>
                                <%--</div>--%>
                              <div class="col-lg-4">
                                <label>Relationship</label>

                                <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-heart"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Relationship"
                                         id="relationship"
                                         name="relationship"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Kin Phone Number</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-mobile-phone"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Phone Number"
                                         id="kin_phone" name="kin_phone"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Crime Committed</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-user-md"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Crime Committed"
                                         id="crime_committed" name="crime_committed"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Duration in Months</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-clock-o"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Sentence Duration"
                                         id="duration" name="duration"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Sentence Start Date</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-calendar"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Start Date"
                                         id="sentence_start_date" name="sentence_start_date"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Sentencing Court</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-home"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Sentencing Court"
                                         id="court" name="court"
                                         value="" disabled/>
                                </div>
                              </div>
                              <div class="col-lg-4">
                                <label>Comments</label>

                                <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-bars"></i></span>
                                  <input class="form-control" type="text"
                                         placeholder="Comments"
                                         id="comments" name="comments"
                                         value="" disabled/>
                                </div>
                              </div>
                            </div>
                            <br/>
                          </div>
                          <div class="modal-footer">
                            <p class="text-center small-box-footer">Copyrights &copy; 2015 PRISM |
                              Designed &amp;Maintained
                              by : <a rel="nofollow"
                                      href="http://www.mimi.com"
                                      target="_blank">Mimi Technology</a> <a
                                      rel="nofollow"
                                      href=""></a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                    </tbody>

                  </table>
                </c:if>
                <p>
                <h4>
                  Printed by:${prismAdminName} </br>
                  on <p id="demo"></p>
                  <script>
                    document.getElementById("demo").innerHTML = Date();
                  </script>
                  </br>
                  Signature: ..............................
                </h4>
                </p>
              </div>
            </div>
            <div class="panel-footer"> </div>

          </div>
        </div>
        <!-- /.row -->
        <!-- Modal -->
        <%--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
        <%--<div class="modal-dialog">--%>
        <%--<div class="modal-content">--%>
        <%--<div class="modal-header">--%>
        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
        <%--<h4 class="modal-title" id="myModalLabel">Staff Details</h4>--%>
        <%--</div>--%>
        <%--<div class="modal-body">--%>
        <%--<table class="table table-bordered" id="dataTables-example">--%>
        <%--<thead>--%>
        <%--<tr>--%>
        <%--<td>Staff ID</td>--%>
        <%--<td>Staff Names</td>--%>
        <%--<td>Gender</td>--%>
        <%--<td>Staff Role</td>--%>
        <%--<td>Others</td>--%>
        <%--</tr>--%>
        <%--</thead>--%>
        <%--<tbody>--%>
        <%--<tr>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--</tr>--%>
        <%--</tbody>--%>
        <%--</table>--%>
        <%--</div>--%>
        <%--<div class="modal-footer">--%>
        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
        <%--</div>--%>
      </div>
    </div>
  </div>
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  $(document).ready(function() {
    $('#dataTables-example').dataTable();
  });
</script>
<%
  } catch (Exception e) {
    System.out.println("wrong entry" + e);
  }
%>
</body>

</html>
