<%--
  Created by IntelliJ IDEA.
  User: kenwambua
  Date: 2/21/2015
  Time: 12:26 PM
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Kenya Service Prison</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <!-- Core CSS - Include with every page -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="${pageContext.request.contextPath}/resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="${pageContext.request.contextPath}/resources/css/sb-admin.css" rel="stylesheet">
    <!-- Core Scripts - Include with every page -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.10.2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/plugins/dataTables/dataTables.bootstrap.js"></script>
  <c:set var="contextPath" value="${pageContext.request.contextPath}" />
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_error_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script type="text/javascript">
    window.setTimeout(function () {
      $("#viewGroup_message_alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
      });
    }, 2500);
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('a[data-confirm]').click(function (ev) {
        var href = $(this).attr('href');
        if (!$('#dataConfirmModal').length) {
          $('body').append('<div class="modal fade" id="dataConfirmModal" tabindex="-1" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true">' +
          '<div class="modal-dialog modal-sm"><div class="modal-content">' +
          '<div class="modal-header bg-blue"><h4 class="modal-title center" id="myModalLabel">Please Confirm</h4></div>' +
          '<div class="modal-body center"></div><div class="modal-footer"><a class="btn btn-danger btn-sm" id="dataConfirmOK">' +
          '<i class="ion ion-ios7-trash"></i> Delete</a><button class="btn btn-info btn-sm" data-dismiss="modal" aria-hidden="true"><i class="fa fa-remove"></i> Cancel</button></div></div></div></div>');
        }
        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show: true});
        return false;
      });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
      $(".viewdata").click(function () { // Click to only happen on announce links
        $("#warden_Number").val($(this).data('warden_number'));
        $("#id_number").val($(this).data('id_number'));
        $("#surname").val($(this).data('surname'));
        $("#other_Names").val($(this).data('other_names'));
        $("#phone_Number").val($(this).data('phone_Number'));
        $("#ROLE").val($(this).data('role'));
        $("#kin_Id").val($(this).data('kin_id'));
        $("#kin_Surname").val($(this).data('kin_surname'));
        $("#kin_Other_Name").val($(this).data('kin_other_name'));
        $("#kin_phone").val($(this).data('kin_phone'));
        $("#kin_Relationship").val($(this).data('kin_relationship'));
//        $("#district").val($(this).data('district'));
//        $("#location").val($(this).data('location'));
//        $("#kin_id").val($(this).data('kin_id'));
//        $("#kin_surname").val($(this).data('kin_surname'));
//        $("#kin_other_names").val($(this).data('kin_other_names'));
//        $("#relationship").val($(this).data('relationship'));
//        $("#kin_phone").val($(this).data('kin_phone'));
//        $("#crime_committed").val($(this).data('crime_committed'));
//        $("#offence_nature").val($(this).data('offence_nature'));
//        $("#duration").val($(this).data('duration'));
//        $("#sentence_start_date").val($(this).data('sentence_start_date'));
//        $("#court").val($(this).data('court'));
//        $("#comments").val($(this).data('comments'));
        $('#dataViewModal').modal('show');
      });
    });
  </script>
</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/prismAdmin">Kenya Prison Service</a>
        </div>

        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <span>${prismAdminName}  <i class="caret"></i></span>
                    <%--<i class="fa fa-caret-down"></i>--%>
                </a>
                <ul class="dropdown-menu dropdown-user">


                    <li><a href="${logout}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <%--<li class="sidebar-search">--%>
                    <%--<div class="input-group custom-search-form">--%>
                    <%--<input type="text" class="form-control" placeholder="Search...">--%>
                    <%--<span class="input-group-btn">--%>
                    <%--<button class="btn btn-default" type="button">--%>
                    <%--<i class="fa fa-search"></i>--%>
                    <%--</button>--%>
                    <%--</span>--%>
                    <%--</div>--%>
                    <%--<!-- /input-group -->--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prismAdmin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/add_users"><i class="fa fa-table fa-fw"></i> Register User</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/view_Inmates"><i class="fa fa-dashboard fa-fw"></i>Inmate Records</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/reports"><i class="fa fa-table fa-fw"></i> Reports<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="${contextPath}/prismAdmin/view_Inmates_Reports">Inmate</a>
                                <a href="${contextPath}/prismAdmin/redirect_report">Personal</a>
                                <a href="${contextPath}/prismAdmin/view_Assault_Reports">Assault Report</a>
                                <a href="${contextPath}/prismAdmin/view_Arson_Reports">Arson</a>
                                <a href="${contextPath}/prismAdmin/view_Burlgary_Reports">Burlgary</a>
                                <a href="${contextPath}/prismAdmin/view_Homicide_Reports">Homicide</a>


                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/view_Visitor"><i class="fa fa-dashboard fa-fw"></i>Visitor Record</a>
                    </li>
                    <%--<li>--%>
                    <%--<a href=""><i class="fa fa-table fa-fw"></i>Parole Requests</a>--%>
                    <%--</li>--%>
                    <li>
                        <a href="${contextPath}/prismAdmin/manage_users"><i class="fa fa-dashboard fa-fw"></i>Manage Users</a>
                    </li>
                    <li>
                        <a href="${contextPath}/prismAdmin/release_Inmates"><i class="fa fa-table fa-fw"></i>Release Inmate</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Manage Users</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Users</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <c:if test="${not empty error}">
                                <div class="alert alert-danger" id="viewGroup_error_alert"><i
                                        class="fa fa-ban"></i> ${error}</div>
                            </c:if>
                            <c:if test="${not empty message}">
                                <div class="alert alert-success" id="viewGroup_message_alert"><i
                                        class="fa fa-check"></i> ${message}</div>
                            </c:if>
                            <br/>
                            <c:if test="${!empty listUser}">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>warden_Number</th>
                                        <th>id_number</th>
                                        <th>surname</th>
                                        <th>other_Names</th>
                                        <th>Phone_Number</th>
                                        <th>Role</th>
                                        <%--<th>Kin_Surname</th>--%>
                                        <%--<th>Kin_Other_Names</th>--%>
                                        <%--<th>Kin_Phone Number</th>--%>
                                        <%--<th>Relationship</th>--%>
                                        <th width="50px"></th>
                                        <th width="50px">Edit</th>
                                        <th width="50px">Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listUser}" var="user">
                                        <tr>
                                            <td>${user.warden_Number}</td>
                                            <td>${user.id_number}</td>
                                            <td>${user.surname}</td>
                                            <td>${user.other_Names}</td>
                                            <td>${user.phone_Number}</td>
                                            <td>${user.ROLE}</td>
                                            <%--<td>${user.warden_Number}</td>--%>
                                            <%--<td>${user.id_number}</td>--%>
                                            <%--<td>${user.surname}</td>--%>
                                            <%--<td>${user.other_Names}</td>--%>
                                            <td width="50px">
                                                <a href="#"
                                                   class="btn btn-custom btn-sm viewdata"
                                                   data-warden_Number="${user.warden_Number}"
                                                   data-id_number="${user.id_number}"
                                                   data-surname="${user.surname}"
                                                   data-other_Names="${user.other_Names}"
                                                   data-phone_Number="${user.phone_Number}"
                                                   data-ROLE="${user.ROLE}"
                                                   data-kin_Id="${user.kin_Id}"
                                                   data-kin_Surname="${user.kin_Surname}"
                                                   data-kin_Other_Name="${user.kin_Other_Name}"
                                                   data-kin_phone="${user.kin_phone}"
                                                   data-kin_Relationship="${user.kin_Relationship}">
                                                    <i class="fa fa-search fa-fw"></i>
                                                    View More</a></td>
                                            <td width="50px" >
                                                 <span class="delete btn-danger"><a href="<c:url value='/prismAdmin/manage_users/delete/${user.warden_Number}' /> " class="btn btn-custom btn-sm"
                                                                                    data-confirm="Are you sure you want to delete?" ><i class="fa fa-edit"></i></a></span>
                                            </td>
                                            <td width="50px" >
                <span class="delete btn-danger"><a href="<c:url value='/prismAdmin/manage_users/delete/${user.warden_Number}' /> " class="btn btn-custom btn-danger"
                                                   data-confirm="Are you sure you want to delete?" ><i class="fa fa-trash-o"></i></a></span>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    <div class="modal fade" id="dataViewModal" tabindex="-1" role="dialog"
                                         aria-labelledby="dataViewModal" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header  bg-blue">
                                                    <a href="#" class="btn btn-danger btn-sm closebtn" aria-hidden="true"
                                                       data-dismiss="modal"><i class="ion ion-ios7-trash"></i> Close</a>
                                                    <h4 class="modal-title center" id="myModalLabel">User Details</h4>
                                                </div>
                                                <div class="modal-body" id="modalview">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="input-group">
                              <span class="input-group-addon hidden">
                                <i class="fa fa-user"></i></span>
                                                                <input type="hidden" name="baptismId" class="form-control"
                                                                       placeholder="baptism Id" id="baptismId"
                                                                       value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="input-group">
                              <span class="input-group-addon hidden">
                                <i class="fa fa-user"></i></span>
                                                                <input type="hidden" name="baptismUuid" class="form-control"
                                                                       placeholder="baptism Uuid" id="baptismUuid"
                                                                       value=""/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="input-group">
							<span class="input-group-addon hidden">
                              <i class="fa fa-calendar"></i></span>
                                                                <input type="hidden" name="baptismDateCreated"
                                                                       class="form-control"
                                                                       placeholder="baptism Date Created"
                                                                       id="baptismDateCreated"
                                                                       value=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <label>Warden Number</label>

                                                            <div class="input-group">
						  <span class="input-group-addon">
                            <i class="fa fa-user"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Warden Number"
                                                                       id="warden_Number" name="warden_Number"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>ID Number</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-user"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="ID Number"
                                                                       id="id_number" name="id_number"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Surname</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-calendar"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Surname"
                                                                       id="surname" name="surname"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>

                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <label>Other Names</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-calendar"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="First Name"
                                                                       id="other_Names" name="other_Names"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Phone Number</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-male"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="phone number"
                                                                       id="phone_Number" name="phone_Number"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Role</label>

                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-female"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Role"
                                                                       id="ROLE" name="ROLE"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>

                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <label>Kin ID</label>

                                                            <div class="input-group">
                              <span class="input-group-addon">
                                <i class="fa fa-user-md"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Kin ID"
                                                                       id="kin_Id" name="kin_Id"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Kin Surname</label>

                                                            <div class="input-group">
					<span class="input-group-addon">
                      <i class="fa fa-envelope"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Kin Surname"
                                                                       id="kin_Surname" name="kin_Surname"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>kin_Other_Name</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-mobile-phone"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="kin_Other_Name"
                                                                       id="kin_Other_Name" name="kin_Other_Name"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>

                                                    <div class="row">
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>kin_Relationship</label>--%>

                                                            <%--<div class="input-group">--%>
						<%--<span class="input-group-addon">--%>
                            <%--<i class="fa fa-mobile-phone"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="kin_Relationship"--%>
                                                                       <%--id="kin_Relationship" name="kin_Relationship"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <div class="col-lg-4">
                                                            <label>kin_phone</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-mobile-phone"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="kin_phone"
                                                                       id="kin_phone" name="kin_phone"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Relationship</label>

                                                            <div class="input-group">
							<span class="input-group-addon">
                              <i class="fa fa-question-circle"></i></span>
                                                                <input class="form-control" type="text"
                                                                       placeholder="Relationship"
                                                                       id="kin_Relationship"
                                                                       name="kin_Relationship"
                                                                       value="" disabled/>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <br/>

                                                    <%--<div class="row">--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Kin other Name</label>--%>

                                                            <%--<div class="input-group">--%>
							<%--<span class="input-group-addon">--%>
                              <%--<i class="fa fa-question-circle"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Other Names"--%>
                                                                       <%--id="kin_other_names"--%>
                                                                       <%--name="kin_other_names"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Reelationship</label>--%>

                                                            <%--<div class="input-group">--%>
							<%--<span class="input-group-addon">--%>
                              <%--<i class="fa fa-question-circle"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Relationship"--%>
                                                                       <%--id="relationship"--%>
                                                                       <%--name="relationship"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Kin Phone Number</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Phone Number"--%>
                                                                       <%--id="kin_phone" name="kin_phone"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Crime Committed</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Crime Committed"--%>
                                                                       <%--id="crime_committed" name="crime_committed"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Duration in Months</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Sentence Duration"--%>
                                                                       <%--id="duration" name="duration"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Sentence Start Date</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Start Date"--%>
                                                                       <%--id="sentence_start_date" name="sentence_start_date"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Sentencing Court</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Sentencing Court"--%>
                                                                       <%--id="court" name="court"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="col-lg-4">--%>
                                                            <%--<label>Comments</label>--%>

                                                            <%--<div class="input-group">--%>
                              <%--<span class="input-group-addon">--%>
                                <%--<i class="fa fa-user-md"></i></span>--%>
                                                                <%--<input class="form-control" type="text"--%>
                                                                       <%--placeholder="Comments"--%>
                                                                       <%--id="comments" name="comments"--%>
                                                                       <%--value="" disabled/>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                    <%--</div>--%>
                                                    <br/>
                                                </div>
                                                <div class="modal-footer">
                                                    <p class="text-center small-box-footer">Copyrights &copy; 2015 PRISM |
                                                        Designed &amp;Maintained
                                                        by : <a rel="nofollow"
                                                                href="http://www.mimi.com"
                                                                target="_blank">Mimi Technology</a> <a
                                                                rel="nofollow"
                                                                href=""></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </tbody>
                                </table>
                            </c:if>
                        </div>
                    </div>
                    <div class="panel-footer"> </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- Modal -->
        <%--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--%>
        <%--<div class="modal-dialog">--%>
        <%--<div class="modal-content">--%>
        <%--<div class="modal-header">--%>
        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
        <%--<h4 class="modal-title" id="myModalLabel">Staff Details</h4>--%>
        <%--</div>--%>
        <%--<div class="modal-body">--%>
        <%--<table class="table table-bordered" id="dataTables-example">--%>
        <%--<thead>--%>
        <%--<tr>--%>
        <%--<td>Staff ID</td>--%>
        <%--<td>Staff Names</td>--%>
        <%--<td>Gender</td>--%>
        <%--<td>Staff Role</td>--%>
        <%--<td>Others</td>--%>
        <%--</tr>--%>
        <%--</thead>--%>
        <%--<tbody>--%>
        <%--<tr>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--<td></td>--%>
        <%--</tr>--%>
        <%--</tbody>--%>
        <%--</table>--%>
        <%--</div>--%>
        <%--<div class="modal-footer">--%>
        <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
        <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
        <%--</div>--%>
    </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- SB Admin Scripts - Include with every page -->
<script src="${pageContext.request.contextPath}/resources/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<script src="${pageContext.request.contextPath}/resources/js/demo/dashboard-demo.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>

</body>

</html>



