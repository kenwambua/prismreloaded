/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.prism_project.Generators;

import java.util.UUID;

/**
 * Created by benjamin on 12/13/14.
 */
public class UuidGen {
	public String uuidGen() {
		String uuid = UUID.randomUUID().toString();
		return uuid;
	}
}
