package com.prism_project.service.Impl;
import com.prism_project.dao.UserDAO;
import com.prism_project.models.User;
import com.prism_project.service.UserService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kenwambua on 2/13/2015.
 */
public class UserServiceImpl implements UserService {
	private UserDAO usersDAO;

	public void setUsersDAO(UserDAO usersDAO) {
		this.usersDAO= usersDAO;
	}

	@Override @Transactional public void addUsers(User user) {
		this.usersDAO.addUsers(user);
	}

	@Override @Transactional public void updateUser(User user) {
		this.usersDAO.updateUser(user);
	}

	@Override @Transactional public List<User> listUser() {
		return this.usersDAO.listUser();
	}

	@Override @Transactional public User getUserByUuid(String userUuid) {return this.getUserByUuid(userUuid);
	}

	@Override @Transactional public void removeUser(int warden_Number) {
		this.usersDAO.removeUser(warden_Number);
	}
}
