package com.prism_project.service.Impl;

import com.prism_project.dao.ActivityDAO;
import com.prism_project.models.Activity;
import com.prism_project.service.ActivityService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public class ActivityServiceImpl implements ActivityService {
	private ActivityDAO activityDAO;

	public void setActivityDAO(ActivityDAO activityDAO) {
		this.activityDAO= activityDAO;
	}

	@Override @Transactional public void addActivity(Activity activity) {
		this.activityDAO.addActivity(activity);
	}

	@Override @Transactional public void updateActivity(Activity activity) {
		this.activityDAO.updateActivity(activity);
	}

	@Override @Transactional public List<Activity> listActivity() {
		return this.activityDAO.listActivity();
	}

	@Override @Transactional public Activity getActivityByUuid(String userUuid) {return this.getActivityByUuid(userUuid);
	}

	@Override @Transactional public void removeActivity(int warden_Number) {
		this.activityDAO.removeActivity(warden_Number);
	}
}
