package com.prism_project.service.Impl;

import com.prism_project.dao.InmateDAO;
import com.prism_project.models.Inmate;
import com.prism_project.service.InmatesService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public class InmatesServiceImpl implements InmatesService {
	private InmateDAO inmateDAO;

	public void setInmateDAO(InmateDAO inmateDAO) {
		this.inmateDAO= inmateDAO;
	}

	@Override @Transactional public void addInmate(Inmate inmate) {
		this.inmateDAO.addInmate(inmate);
	}

	@Override @Transactional public void updateInmate(Inmate inmate) {
		this.inmateDAO.updateInmate(inmate);
	}

	@Override @Transactional public List<Inmate> listInmate() {
		return this.inmateDAO.listInmate();
	}

	@Override @Transactional public Inmate getInmateById(int prisonerId) {
		return this.getInmateById(prisonerId);
	}

	@Override @Transactional public void removeInmate(int prisonerId) {
		this.inmateDAO.removeInmate(prisonerId);
	}
}


