package com.prism_project.service.Impl;


import com.prism_project.dao.VisitorDAO;
import com.prism_project.models.Visitor;
import com.prism_project.service.VisitorService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public class VisitorServiceImpl implements VisitorService {
	private VisitorDAO visitorDAO;

	public void setVisitorDAO(VisitorDAO visitorDAO) {
		this.visitorDAO=visitorDAO;
	}

	@Override @Transactional public void addVisitor(Visitor visitor) {this.visitorDAO.addVisitor(visitor);}

	@Override @Transactional public void updateVisitor(Visitor visitor) {this.visitorDAO.updateVisitor(visitor);}

	@Override @Transactional public List<Visitor> listVisitor() {
		return this.visitorDAO.listVisitor();
	}

	@Override @Transactional public Visitor getVisitorByid(String visitor_id) {return this.getVisitorByid(visitor_id);
	}

	@Override @Transactional public void removeVisitor(int visitor_id) {
		this.visitorDAO.removeVisitor(visitor_id);
	}
}



