package com.prism_project.service;

import com.prism_project.models.Activity;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public interface ActivityService {
	public void addActivity(Activity activity);

	public void updateActivity(Activity activity);

	public List<Activity> listActivity();

	public Activity getActivityByUuid(String userUuid);

	public void removeActivity(int warden_Number);

}
