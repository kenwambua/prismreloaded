package com.prism_project.service;

import com.prism_project.models.Inmate;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public interface InmatesService {
	public void addInmate(Inmate inmate);

	public void updateInmate(Inmate inmate);

	public List<Inmate> listInmate();

	public Inmate getInmateById(int prisonerId);

	public void removeInmate(int prisonerId);

}
