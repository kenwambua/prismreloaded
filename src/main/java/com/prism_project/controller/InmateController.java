package com.prism_project.controller;

import com.prism_project.models.Inmate;
import com.prism_project.service.InmatesService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

/**
 * Created by kenwambua on 2/27/2015.
 */
@Controller
public class InmateController {
	private static final Logger logger = Logger.getLogger(InmateController.class);
	private InmatesService inmateService;

	@Autowired(required = true) @Qualifier(value = "inmateService")
	public void setInmateService(InmatesService inmateService1) {this.inmateService = inmateService1;}

	@RequestMapping(value = "/prism_User/register_inmates/save", method = RequestMethod.POST)
	public String addInmate(@ModelAttribute("inmate_records") Inmate inmate, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			if (inmate.getId_number() != 0) {
				this.inmateService.addInmate(inmate);
				redirectAttributes.addFlashAttribute("message", "Inmate added successfully");
			} else {
				this.inmateService.updateInmate(inmate);
				redirectAttributes.addFlashAttribute("message", "Inmate updated successfully");
			}

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "User not saved");
		}

		return "redirect:/prism_User/register_inmate";
	}
	@RequestMapping(value = "/prism_Admin/register_inmates/save", method = RequestMethod.POST)
	public String updateInmate(@ModelAttribute("inmate_records") Inmate inmate, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {

				this.inmateService.updateInmate(inmate);
				redirectAttributes.addFlashAttribute("message", "Inmate updated successfully");
			

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "User not saved");
		}

		return "redirect:/prismAdmin/release_Inmates";
	}
	@RequestMapping(value = "/prism_User/view_Inmates", method = RequestMethod.GET)
	public ModelAndView listInmate(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismUser/inmate_records");
		return viewInmatesModel;
	}
//	@RequestMapping(value = "/prism_User/view_Inmates_Report", method = RequestMethod.GET)
//	public ModelAndView listInmateReport(ModelMap modelMap, Principal principal) {
//		String prismUserName = principal.getName();
//		modelMap.addAttribute("prismUserName", prismUserName);
//		modelMap.addAttribute("inmate", new Inmate());
//		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
//		ModelAndView viewInmatesModel = new ModelAndView();
//		viewInmatesModel.addObject(modelMap);
//		viewInmatesModel.addObject("message", "Fetched inmates successfully");
//		viewInmatesModel.setViewName("prismAdmin/prison_Reports");
//		return viewInmatesModel;
//	}
	@RequestMapping(value = "/prism_User/add_Visitors", method = RequestMethod.GET)
	public ModelAndView listInmates(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmates", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismUser/visitor");
		return viewInmatesModel;
	}
//	@RequestMapping(value = "/prismAdmin/manage_users", method = RequestMethod.GET)
//	public ModelAndView listUser(ModelMap modelMap, Principal principal) {
//		String prismAdminName = principal.getName();
//		modelMap.addAttribute("prismAdminName", prismAdminName);
//		modelMap.addAttribute("user", new ser());
//		modelMap.addAttribute("listUser", this.userService.listUser());
//		ModelAndView userModel = new ModelAndView();
//		userModel.addObject(modelMap);
//		//		userModel.addObject("message", "Fetched users successfully.");
//		userModel.setViewName("prismAdmin/manage_user");
//		return  userModel;
//	@RequestMapping(value = "/prismAdmin/manage_users", method = RequestMethod.GET)
//	public ModelAndView listInmate(ModelMap modelMap, Principal principal) {
//		String prism_UserName = principal.getName();
//		modelMap.addAttribute("prism_UserName", prism_UserName);
//		modelMap.addAttribute("inmate", new Inmate());
//		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
//		ModelAndView userModel = new ModelAndView();
//		userModel.addObject(modelMap);
//		//		userModel.addObject("message", "Fetched users successfully.");
//		userModel.setViewName("prismAdmin/manage_user");
//		return  userModel;
//	}
//	@RequestMapping("/prismAdmin/manage_users/delete/{warden_Number}")
//	public String removeInmate(@PathVariable("warden_Number") String prisonerId,
//			RedirectAttributes redirectAttributes) throws RuntimeException {
//		try {
//			this.inmateService.removeInmate(prisonerId);
//			redirectAttributes.addFlashAttribute("message", "User was deleted successfully");
//		} catch (RuntimeException ex) {
//			redirectAttributes.addFlashAttribute("error", "User was not deleted");
//		}
//
//		return "redirect:/prismAdmin/manage_users";
//	}

}


