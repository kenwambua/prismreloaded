package com.prism_project.controller;

import com.prism_project.models.Inmate;
import com.prism_project.models.Visitor;
import com.prism_project.service.VisitorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

/**
 * Created by kenwambua on 3/11/2015.
 */
@Controller
public class VisitorController {
	private static final Logger logger = Logger.getLogger(InmateController.class);
	private VisitorService visitorService;

	@Autowired(required = true) @Qualifier(value = "visitorService")
	public void setVisitorService(VisitorService visitorService1) {
		this.visitorService = visitorService1;
	}

	@RequestMapping(value = "/prism_User/register_visitor/save", method = RequestMethod.POST)
	public String addInmate(@ModelAttribute("inmate_records") Visitor visitor, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			this.visitorService.addVisitor(visitor);
			redirectAttributes.addFlashAttribute("message", "Visitor updated successfully");
		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "Visitor not saved");
		}

		return "redirect:/prism_User/add_Visitors";
	}

	@RequestMapping(value = "/prism_User/view_Visitors", method = RequestMethod.GET)
	public ModelAndView listVisitors(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		modelMap.addAttribute("visitor", new Visitor());
		modelMap.addAttribute("listVisitor", this.visitorService.listVisitor());
		ModelAndView viewVisitorsModel = new ModelAndView();
		viewVisitorsModel.addObject(modelMap);
		viewVisitorsModel.addObject("message", "Fetched Visitors successfully");
		viewVisitorsModel.setViewName("prismUser/visitor_records");
		return viewVisitorsModel;
	}
	@RequestMapping(value = "/prismAdmin/view_Visitor", method = RequestMethod.GET)
	public ModelAndView listVisitor(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismUserName", prismAdminName);
		modelMap.addAttribute("visitor", new Visitor());
		modelMap.addAttribute("listVisitor", this.visitorService.listVisitor());
		ModelAndView viewVisitorsModel = new ModelAndView();
		viewVisitorsModel.addObject(modelMap);
		viewVisitorsModel.addObject("message", "Fetched visitors successfully");
		viewVisitorsModel.setViewName("prismAdmin/visitor_records");
		return viewVisitorsModel;
	}
	@RequestMapping(value = "/prism_User/add_Visitor/save", method = RequestMethod.POST)
	public String addVisitor(@ModelAttribute("add_visitor") Visitor visitor, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			if (visitor.getId_number() != 0) {
				this.visitorService.addVisitor(visitor);
				redirectAttributes.addFlashAttribute("message", "Visitor added successfully");
			} else {
				this.visitorService.updateVisitor(visitor);
				redirectAttributes.addFlashAttribute("message", "Visitor updated successfully");
			}

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "Visitor not saved");
		}

		return "redirect:/prism_User/add_Visitors";
	}
//	@RequestMapping(value = "/prism_User/view_Visitorss", method = RequestMethod.GET)
//	public ModelAndView listVisitor(ModelMap modelMap, Principal principal) {
//		String prismUserName = principal.getName();
//		modelMap.addAttribute("prismUserName", prismUserName);
//		modelMap.addAttribute("visitor", new Inmate());
//		modelMap.addAttribute("listVisitor", this.visitorService.listVisitor());
//		ModelAndView viewVisitorModel = new ModelAndView();
//		viewVisitorModel.addObject(modelMap);
//		viewVisitorModel.addObject("message", "Fetched Visitors successfully");
//		viewVisitorModel.setViewName("prismUser/visitor_records");
//		return viewVisitorModel;
//	}
}
