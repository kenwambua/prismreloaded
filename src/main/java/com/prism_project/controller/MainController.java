/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.prism_project.controller;

import com.prism_project.models.Inmate;
import com.prism_project.models.User;
import com.prism_project.service.InmatesService;
import com.prism_project.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller public class MainController {

	private static final Logger logs = Logger.getLogger(MainController.class);
	private InmatesService inmateService;
	@Autowired(required = true) @Qualifier(value = "inmateService")
	public void setInmateService(InmatesService inmateService1) {
		this.inmateService = inmateService1;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView getLoginForm(@RequestParam(required = false) String authfailed, String logout, String denied, String sessionExpired) {

		ModelAndView model = new ModelAndView();
		if (authfailed != null) {
			model.addObject("error", "Invalid login credentials");
			logs.info("Invalid username or password, try again !");
		} else if (logout != null) {
			model.addObject("message", "You've been logged out successfully.");
			logs.info("Logged Out successfully");
		}
		else if (sessionExpired != null) {
			model.addObject("error", "User Session Expired Please Login to Continue");
			logs.info("User Session Expired");
		}
		if (denied != null) {
			model.addObject("error", "Access Denied");
			logs.info("User access denied");
		}
		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/prism_User", method = RequestMethod.GET)
	public ModelAndView getPrismUserPage(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		ModelAndView prismUserModel = new ModelAndView();
		prismUserModel.addObject(modelMap);
		prismUserModel.setViewName("prism_User");
		return  prismUserModel;
	}

	@RequestMapping(value = "/prismAdmin", method = RequestMethod.GET)
	public ModelAndView getSchoolAdmin(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView prismAdminModel = new ModelAndView();
		prismAdminModel.addObject(modelMap);
		prismAdminModel.setViewName("prismAdmin");
		return prismAdminModel;
	}

	@RequestMapping(value = "/error403", method = RequestMethod.GET) 
	public String getErrorPage() {
		return "error403";
	}

	@RequestMapping(value = "/prism_User/register_inmate", method = RequestMethod.GET)
	public ModelAndView registerInmates(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		ModelAndView registerInmatesModel = new ModelAndView();
		registerInmatesModel.addObject(modelMap);
		registerInmatesModel.setViewName("prismUser/register");
		return registerInmatesModel;
	}
	@RequestMapping(value = "/prismAdmin/add_users", method = RequestMethod.GET)
	public ModelAndView addUsers(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView addUserModel = new ModelAndView();
		addUserModel.addObject(modelMap);
		addUserModel.setViewName("prismAdmin/add_user");
		return addUserModel;
	}
	@RequestMapping(value = "/prism_User/add_Activity", method = RequestMethod.GET)
	public ModelAndView addActivity(ModelMap modelMap, Principal principal) {
		String prismUserName = principal.getName();
		modelMap.addAttribute("prismUserName", prismUserName);
		ModelAndView registerInmatesModel = new ModelAndView();
		registerInmatesModel.addObject(modelMap);
		registerInmatesModel.setViewName("prismUser/add_activity");
		return registerInmatesModel;
	}
	@RequestMapping(value = "/prismAdmin/personal_report", method = RequestMethod.GET)
	public ModelAndView personal_report(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView personalModel = new ModelAndView();
		personalModel.addObject(modelMap);
		personalModel.setViewName("prismAdmin/personal_list");
		return personalModel;
	}
	@RequestMapping(value = "/prismAdmin/redirect_report", method = RequestMethod.GET)
	public ModelAndView redirectReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView personalModel = new ModelAndView();
		personalModel.addObject(modelMap);
		personalModel.setViewName("prismAdmin/redirect");
		return personalModel;
	}
	@RequestMapping(value = "/prismAdmin/view_Inmates", method = RequestMethod.GET)
	public ModelAndView listInmate(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismAdmin/inmate_record");
		return viewInmatesModel;

	}
	@RequestMapping(value = "/prismAdmin/list_P_Inmates", method = RequestMethod.GET)
	public ModelAndView listInmates(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismAdmin/personal_report");
		return viewInmatesModel;

	}
	@RequestMapping(value = "/prismAdmin/list_Inmates", method = RequestMethod.GET)
	public ModelAndView list_Inmate(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismAdmin/personal_list");
		return viewInmatesModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Inmates_Reports", method = RequestMethod.GET)
	public ModelAndView listInmateReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/prison_Reports");
		return viewInmatesReportModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Arson_Reports", method = RequestMethod.GET)
	public ModelAndView listArsonReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/arson");
		return viewInmatesReportModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Burlgary_Reports", method = RequestMethod.GET)
	public ModelAndView listBurlgaryReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/Burlgary");
		return viewInmatesReportModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Assault_Reports", method = RequestMethod.GET)
	public ModelAndView listAssaultReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/Assault");
		return viewInmatesReportModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Homicide_Reports", method = RequestMethod.GET)
	public ModelAndView listHomicideReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/Homicide");
		return viewInmatesReportModel;

	}
	@RequestMapping(value = "/prismAdmin/view_Robbery_Reports", method = RequestMethod.GET)
	public ModelAndView listRobberyReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/Robbery");
		return viewInmatesReportModel;
	}
	@RequestMapping(value = "/prismAdmin/view_Personal_Reports", method = RequestMethod.GET)
	public ModelAndView listPersonalReport(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesReportModel = new ModelAndView();
		viewInmatesReportModel.addObject(modelMap);
		viewInmatesReportModel.addObject("message", "Fetched inmates successfully");
		viewInmatesReportModel.setViewName("prismAdmin/personal_list");
		return viewInmatesReportModel;
	}

	@RequestMapping(value = "/prismAdmin/release_Inmates", method = RequestMethod.GET)
	public ModelAndView releaseInmate(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("inmate", new Inmate());
		modelMap.addAttribute("listInmate", this.inmateService.listInmate());
		ModelAndView viewInmatesModel = new ModelAndView();
		viewInmatesModel.addObject(modelMap);
		viewInmatesModel.addObject("message", "Fetched inmates successfully");
		viewInmatesModel.setViewName("prismAdmin/Release");
		return viewInmatesModel;
	}
	@RequestMapping(value = "/prismAdmin/reports", method = RequestMethod.GET)
	public ModelAndView getReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getReportsModel = new ModelAndView();
		getReportsModel.addObject(modelMap);
		getReportsModel.setViewName("prismAdmin/reports");
		return getReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/prison_report", method = RequestMethod.GET)
	public ModelAndView getPrisonReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/prison_Reports");
		return getPrisonReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/assault_report", method = RequestMethod.GET)
	public ModelAndView getAssaultReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/Assault");
		return getPrisonReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/burlgary_report", method = RequestMethod.GET)
	public ModelAndView getBurlgaryReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/Burlgary");
		return getPrisonReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/homicide_report", method = RequestMethod.GET)
	public ModelAndView getHomicideReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/Homicide");
		return getPrisonReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/robbery_report", method = RequestMethod.GET)
	public ModelAndView getRobberyReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/Robbery");
		return getPrisonReportsModel;
	}
	@RequestMapping(value = "/prismAdmin/report/arson_report", method = RequestMethod.GET)
	public ModelAndView getArsonReports(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		ModelAndView getPrisonReportsModel = new ModelAndView();
		getPrisonReportsModel.addObject(modelMap);
		getPrisonReportsModel.setViewName("prismAdmin/arson");
		return getPrisonReportsModel;
	}

	@RequestMapping(value = "/prismAdmin/release_inmate/{inmate_id}", method = RequestMethod.POST)
	public String addUser(@PathVariable("inmate_id") int inmate_id, Inmate inmate, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			if (inmate.getPrisonerId() == inmate_id) {
				this.inmateService.updateInmate(inmate);
				redirectAttributes.addFlashAttribute("message", "Inmate Released successfully");
			} else {

				redirectAttributes.addFlashAttribute("message", "Failed");
			}

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "Failure");
		}


		return "redirect:/prismAdmin/release_Inmates";
	}
}
