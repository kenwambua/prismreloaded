/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.prism_project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by kenwambua on 12/01/15.
 */
@Controller public class WebController {

	@RequestMapping(value = "/passwordRecovery", method = RequestMethod.GET) public String forgotPasswordPage() {
		return "forgotPassword";
	}
}
