package com.prism_project.controller;

import com.prism_project.models.Activity;
import com.prism_project.models.Inmate;
import com.prism_project.service.ActivityService;
import com.prism_project.service.InmatesService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kenwambua on 3/9/2015.
 */
@Controller
public class ActivityController {
	int trial=123213;
	private InmatesService inmateService;
	@Autowired(required = true) @Qualifier(value = "inmateService")
	public void setInmateService(InmatesService inmateService1) {
		this.inmateService = inmateService1;
	}

	private static final Logger logger = Logger.getLogger(ActivityController.class);
	private ActivityService activityService;

	@Autowired(required = true) @Qualifier(value = "activityService")
	public void setActivityService(ActivityService activityService1) {
		this.activityService = activityService1;
	}

	@RequestMapping(value = "/prism_User/add_activity/save", method = RequestMethod.POST)
	public String addActivity(@ModelAttribute("add_activity") Activity activity,Inmate inmate, RedirectAttributes redirectAttributes)
			throws RuntimeException {

		try {
//				if () {
				this.activityService.addActivity(activity);
				redirectAttributes.addFlashAttribute("message", "Activity added successfully");
//			} else {
//
//				redirectAttributes.addFlashAttribute("message", "Inmate doesn't exist"+ken);
//		}

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "Activity not saved");
		}

		return "redirect:/prism_User/add_Activity";
	}

}
