package com.prism_project.controller;

import com.prism_project.models.User;
import com.prism_project.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

/**
 * Created by kenwambua on 2/16/2015.
 */

@Controller public class UserController {
	private static final Logger logger = Logger.getLogger(UserController.class);
	private UserService userService;

	@Autowired(required = true) @Qualifier(value = "userService")
	public void setUserService(UserService userService1) {
		this.userService = userService1;
	}

	@RequestMapping(value = "/prismAdmin/add_users/save", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("trial_users") User user, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			if (user.getWarden_Number() != 0) {
				this.userService.addUsers(user);
				redirectAttributes.addFlashAttribute("message", "User added successfully");
			} else {
				this.userService.updateUser(user);
				redirectAttributes.addFlashAttribute("message", "User updated successfully");
			}

		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "User not saved");
		}

		return "redirect:/prismAdmin/add_users";
	}
	@RequestMapping(value = "/prismAdmin/manage_users", method = RequestMethod.GET)
	public ModelAndView listUser(ModelMap modelMap, Principal principal) {
		String prismAdminName = principal.getName();
		modelMap.addAttribute("prismAdminName", prismAdminName);
		modelMap.addAttribute("user", new User());
		modelMap.addAttribute("listUser", this.userService.listUser());
		ModelAndView userModel = new ModelAndView();
		userModel.addObject(modelMap);
//		userModel.addObject("message", "Fetched users successfully.");
		userModel.setViewName("prismAdmin/manage_user");
		return  userModel;
	}


	@RequestMapping("/prismAdmin/manage_users/delete/{warden_Number}")
	public String removeUser(@PathVariable("warden_Number") int warden_Number,
			RedirectAttributes redirectAttributes) throws RuntimeException {
		try {
			this.userService.removeUser(warden_Number);
			redirectAttributes.addFlashAttribute("message", "User was deleted successfully");
		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "User was not deleted");
		}

		return "redirect:/prismAdmin/manage_users";
	}

}
