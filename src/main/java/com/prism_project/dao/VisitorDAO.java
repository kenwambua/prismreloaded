package com.prism_project.dao;


import com.prism_project.models.Visitor;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public interface VisitorDAO {
	public void addVisitor(Visitor visitor);

	public void updateVisitor(Visitor visitor);

	public List<Visitor> listVisitor();

	public Visitor getVisitorByid(String visitor_id);

	public void removeVisitor(int visitor_id);
}
