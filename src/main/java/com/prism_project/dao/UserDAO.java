package com.prism_project.dao;

import com.prism_project.models.User;

import java.util.List;


/**
 * Created by kenwambua on 2/13/2015.
 */
public interface UserDAO {
	public void addUsers(User user);

	public void updateUser(User user);

	public List<User> listUser();

	public User getUserByUuid(String userUuid);

	public void removeUser(int warden_Number);
}
