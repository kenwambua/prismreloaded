package com.prism_project.dao;


import com.prism_project.models.Release;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public interface ReleaseDAO {
	public void addRelease(Release release);

	public void updateRelease(Release release);

	public List<Release> listRelease();

	public Release getReleaseByUuid(String userUuid);

	public void removeRelease(int inmate_Number);
}

