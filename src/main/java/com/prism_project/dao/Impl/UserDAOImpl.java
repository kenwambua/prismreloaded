package com.prism_project.dao.Impl;

import com.prism_project.Generators.UuidGen;
import com.prism_project.dao.UserDAO;
import com.prism_project.models.User;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by kenwambua on 2/13/2015.
 */
@Repository public class UserDAOImpl implements UserDAO {
	private static final Logger logger = Logger.getLogger(UserDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override public void addUsers(User user){
	Session session = this.sessionFactory.getCurrentSession();
	try {
		String Uuid = new UuidGen().uuidGen();
		user.setWardenUuid(Uuid);
		session.persist(user);
		logger.info("User saved successfully, User Details=" + user);
	} catch (RuntimeException ex) {
		throw ex;
	}
}
	@Override public void updateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(user);
			logger.info("User saved successfully, User Details=" + user);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@SuppressWarnings("unchecked") @Override public List<User> listUser() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<User> usersList = session.createQuery("from User ").list();
			for (User user : usersList) {
				logger.info("Person List::" + user);
			}
			return usersList;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public User getUserByUuid(String userUuid) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			User user = (User)session.load(User.class, userUuid);
			logger.info("User loaded successfully, User Details=" + user);
			return user;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public void removeUser(int warden_Number) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			User user = (User)session.load(User.class, warden_Number);
			if (null != user) {
				session.delete(user);
			}
			logger.info("User deleted successfully,User Details=" + user);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}
}


