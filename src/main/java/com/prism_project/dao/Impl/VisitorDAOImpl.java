package com.prism_project.dao.Impl;

import com.prism_project.Generators.UuidGen;
import com.prism_project.dao.VisitorDAO;
import com.prism_project.models.Visitor;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
* Created by kenwambua on 2/25/2015.
*/
public class VisitorDAOImpl implements VisitorDAO {
	private static final Logger logger = Logger.getLogger(VisitorDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override public void addVisitor(Visitor visitor){
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			String Uuid = new UuidGen().uuidGen();
//			visitor.set.setWardenUuid(Uuid);
			session.persist(visitor);
			logger.info("User saved successfully, User Details=" + visitor);
		} catch (RuntimeException ex) {
			throw ex;
		}
	}
	@Override public void updateVisitor(Visitor visitor) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(visitor);
			logger.info("User saved successfully, User Details=" + visitor);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@SuppressWarnings("unchecked") @Override public List<Visitor> listVisitor() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Visitor> visitorList = session.createQuery("from Visitor ").list();
			for (Visitor visitor : visitorList) {
				logger.info("Visitor List::" + visitor);
			}
			return visitorList;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public Visitor getVisitorByid(String visitor_id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Visitor visitor = (Visitor)session.load(Visitor.class, visitor_id);
			logger.info("Inmates loaded successfully, Inmate Details=" + visitor);
			return visitor;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public void removeVisitor(int visitor_id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Visitor visitor = (Visitor)session.load(Visitor.class, visitor_id);
			if (null != visitor) {
				session.delete(visitor);
			}
			logger.info("User deleted successfully,User Details=" + visitor);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}
}
