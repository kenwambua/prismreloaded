package com.prism_project.dao.Impl;

import com.prism_project.Generators.UuidGen;
import com.prism_project.dao.ActivityDAO;
import com.prism_project.models.Activity;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public class ActivityDAOImpl implements ActivityDAO {
	private static final Logger logger = Logger.getLogger(ActivityDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override public void addActivity(Activity activity){
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			String Uuid = new UuidGen().uuidGen();
//			activity.setActivitiesUuid(Uuid);
			session.persist(activity);
			logger.info("User saved successfully, User Details=" + activity);
		} catch (RuntimeException ex) {
			throw ex;
		}
	}
	@Override public void updateActivity(Activity user) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(user);
			logger.info("User saved successfully, User Details=" + user);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@SuppressWarnings("unchecked") @Override public List<Activity> listActivity() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Activity> activityList = session.createQuery("from Activity ").list();
			for (Activity activity : activityList) {
				logger.info("Person List::" + activity);
			}
			return activityList;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public Activity getActivityByid(String inmate_id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Activity activity = (Activity)session.load(Activity.class, inmate_id);
			logger.info("User loaded successfully, User Details=" + activity);
			return activity;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public void removeActivity(int inmate_id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Activity activity = (Activity)session.load(Activity.class, inmate_id);
			if (null != activity) {
				session.delete(activity);
			}
			logger.info("Activity deleted successfully,User Details=" + activity);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}
}
