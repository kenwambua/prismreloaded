package com.prism_project.dao.Impl;

import com.prism_project.Generators.UuidGen;
import com.prism_project.dao.InmateDAO;
import com.prism_project.models.Inmate;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public class InmateDAOImpl implements InmateDAO {
	private static final Logger logger = Logger.getLogger(UserDAOImpl.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override public void addInmate(Inmate inmate){
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			String Uuid = new UuidGen().uuidGen();
//			inmate.setInmateUuid(Uuid);
			session.persist(inmate);
			logger.info("User saved successfully, User Details=" + inmate);
		} catch (RuntimeException ex) {
			throw ex;
		}
	}
	@Override public void updateInmate(Inmate inmate) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(inmate);
			logger.info("User saved successfully, User Details=" + inmate);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@SuppressWarnings("unchecked") @Override public List<Inmate> listInmate() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Inmate> inmateList = session.createQuery("from Inmate ").list();
			for (Inmate inmate : inmateList) {
				logger.info("Person List::" + inmate);
			}
			return inmateList;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public Inmate getInmateById(int prisonerId) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Inmate inmate = (Inmate)session.load(Inmate.class, prisonerId);
			logger.info("Inmates loaded successfully, Inmate Details=" + inmate);
			return inmate;
		} catch (RuntimeException ex) {

			throw ex;
		}
	}

	@Override public void removeInmate(int prisonerId) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Inmate inmate = (Inmate)session.load(Inmate.class, prisonerId);
			if (null != inmate) {
				session.delete(inmate);
			}
			logger.info("User deleted successfully,User Details=" + inmate);
		} catch (RuntimeException ex) {

			throw ex;
		}
	}
}
