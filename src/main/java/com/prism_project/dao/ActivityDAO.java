package com.prism_project.dao;

import com.prism_project.models.Activity;
import com.prism_project.models.User;

import java.util.List;

/**
 * Created by kenwambua on 2/25/2015.
 */
public interface ActivityDAO {
	public void addActivity(Activity activity);

	public void updateActivity(Activity activity);

	public List<Activity> listActivity();

	public Activity getActivityByid(String inmate_id);

	public void removeActivity(int inmate_id);
}
