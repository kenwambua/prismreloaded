/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.prism_project.models;

/**
 * Created by kenwambua on 12/23/14.
 */
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "trial_users")
//@SecondaryTable(name= "user_kin")
public class User {
    @Id @Column(name = "warden_Number")  private int warden_Number;
    @Column(name = "wardenUuid")  private String wardenUuid;
    @Column(name = "id_number")  private String id_number;
    @Column(name = "surname")  private String surname;
    @Column(name = "other_Names")  private String other_Names;
    @Column(name = "phone_Number")  private String phone_Number;
    @Column(name = "ROLE")  private String user_Role;
    @Column(name = "password")   private String password;
    @Column(name = "kin_Id")  private String kin_Id;
    @Column(name = "kin_Surname")   private String kin_Surname;
    @Column(name = "kin_Other_Name")  private String kin_Other_Name;
    @Column(name = "kin_phone")  private String kin_phone;
    @Column(name = "kin_Relationship")   private String kin_Relationship;
    @Column(name = "status")  private String status;

    

    public String getWardenUuid() {
        return wardenUuid;
    }

    public void setWardenUuid(String wardenUuid) {
        this.wardenUuid = wardenUuid;
    }

    public int getWarden_Number() {return warden_Number;}

    public void setWarden_Number(int warden_Number) {
        this.warden_Number = warden_Number;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOther_Names() {
        return other_Names;
    }

    public void setOther_Names(String other_Names) {
        this.other_Names = other_Names;
    }

    public String getPhone_Number() {
        return phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        this.phone_Number = phone_Number;
    }

    public String getROLE() {
        return user_Role;
    }

    public void setROLE(String user_Role) {
        this.user_Role = user_Role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    //   @Column(table="user_kin")
    public String getKin_Id() {
        return kin_Id;
    }

    public void setKin_Id(String kin_Id) {
        this.kin_Id = kin_Id;
    }

//    @Column(table="user_kin")
    public String getKin_Surname() {
        return kin_Surname;
    }


    public void setKin_Surname(String kin_Surname) {
        this.kin_Surname = kin_Surname;
    }

//    @Column(table="user_kin")
    public String getKin_Other_Name() {
        return kin_Other_Name;
    }

    public void setKin_Other_Name(String kin_Other_Name) {
        this.kin_Other_Name = kin_Other_Name;
    }

//    @Column(table="user_kin")
    public String getKin_phone() {
        return kin_phone;
    }

    public void setKin_phone(String kin_phone) {
        this.kin_phone = kin_phone;
    }

//    @Column(table="user_kin")
    public String getKin_Relationship() {
        return kin_Relationship;
    }

    public void setKin_Relationship(String kin_Relationship) {
        this.kin_Relationship = kin_Relationship;
    }

//    @Override public String toString() {
//        return "churchGroupId=" + churchGroupId + ",churchGroupUuid" + churchGroupUuid + ", churchGroupName="
//                + churchGroupName + ", churchGroupDesc=" + churchGroupDesc;
//    }
}
