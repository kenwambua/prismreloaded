package com.prism_project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by kenwambua on 2/25/2015.
 */
@Entity
@Table (name="visitors")
public class Visitor {
	@Id @Column(name = "id_number")  private int id_number;
	@Column(name = "surname")  private String surname;
	@Column(name = "other_names")  private String other_names;
	@Column(name = "inmate_visited")  private int inmate_visited;
	@Column(name = "date_of_visit") private Timestamp date_of_visit;
	@Column(name = "phone_number")  private int phone_number;
	@Column(name = "warden")  private int warden;



	public int getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(int phone_number) {
		this.phone_number = phone_number;
	}

	public int getWarden() {return warden;
	}

	public void setWarden(int warden) {
		this.warden = warden;
	}

	public void setDate_of_visit(Timestamp date_of_visit) {
		this.date_of_visit = date_of_visit;
	}

	public Timestamp getDate_of_visit() {
		return date_of_visit;
	}

	public int getId_number() {
		return id_number;
	}

	public void setId_number(int id_number) {
		this.id_number = id_number;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOther_names() {
		return other_names;
	}

	public void setOther_names(String other_names) {
		this.other_names = other_names;
	}

	public int getInmate_visited() {
		return inmate_visited;
	}

	public void setInmate_visited(int inmate_visited) {
		this.inmate_visited = inmate_visited;
	}


	@Override public String toString() {
		return "Visitor{" +
				"id_number=" + id_number +
				", surname='" + surname + '\'' +
				", other_names='" + other_names + '\'' +
				", inmate_visited='" + inmate_visited + '\'' +
				", date_of_visit='" + date_of_visit + '\'' +
				", phone_number='" + phone_number + '\'' +
				'}';
	}
}
