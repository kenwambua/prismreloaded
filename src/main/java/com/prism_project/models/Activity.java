package com.prism_project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by kenwambua on 2/25/2015.
 */
@Entity @Table(name = "inmate_activities") public class Activity {
	@Id @Column(name = "inmate_id")  private int inmate_id;
	@Column(name = "comment")  private String comment;
	@Column(name = "cartegory")  private String cartegory;
	@Column(name = "warden_id")  private int warden_id;
	@Column(name = "activity_date")  private Timestamp activity_date;

	public int getInmate_id() {
		return inmate_id;
	}

	public void setInmate_id(int inmate_id) {
		this.inmate_id = inmate_id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCartegory() {
		return cartegory;
	}

	public void setCartegory(String cartegory) {
		this.cartegory = cartegory;
	}

	public int getWarden_id() {
		return warden_id;
	}

	public void setWarden_id(int warden_id) {
		this.warden_id = warden_id;
	}

	public Timestamp getActivity_date() {
		return activity_date;
	}

	public void setActivity_date(Timestamp activity_date) {
		this.activity_date = activity_date;
	}
}
