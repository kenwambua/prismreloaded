package com.prism_project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by kenwambua on 2/25/2015.
 */
@Entity @Table(name = "inmate_records") public class Inmate {
	@Id @Column(name = "prisonerId") private int prisonerId;
	@Column(name = "id_number") private int id_number;
//	@Column(name = "inmateUuid") private String inmateUuid;
	@Column(name = "surname") private String surname;
	@Column(name = "firstname") private String firstname;
	@Column(name = "other_name") private String other_name;
	@Column(name = "dob") private String dob;
	@Column(name = "weight") private int weight;
	@Column(name = "height") private int height;
	@Column(name = "complexion") private String complexion;
//	@Column(name = "town") private String town;
	@Column(name = "county") private String county;
//	@Column(name = "district") private String district;
	@Column(name = "location") private String location;
	@Column(name = "kin_id") private int kin_id;
	@Column(name = "kin_surname") private String kin_surname;
	@Column(name = "kin_other_names") private String kin_other_names;
	@Column(name = "relationship") private String relationship;
	@Column(name = "kin_phone") private String kin_phone;
	@Column(name = "crime_committed") private String crime_committed;
	@Column(name = "offence_nature") private String offence_nature;
	@Column(name = "duration") private String duration;
	@Column(name = "sentence_start_date") private String sentence_start_date;
	@Column(name = "release_date") private String release_date;
	@Column(name = "court") private String court;
	@Column(name = "comments") private String comments;
	@Column(name = "status") private String status;

	public int getPrisonerId() {
		return prisonerId;
	}

	public void setPrisonerId(int prisonerId) {
		this.prisonerId = prisonerId;
	}

	public int getId_number() {
		return id_number;
	}

	public void setId_number(int id_number) {
		this.id_number = id_number;
	}

//	public String getInmateUuid() {
//		return inmateUuid;
//	}
//
//	public void setInmateUuid(String inmateUuid) {
//		this.inmateUuid = inmateUuid;
//	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getOther_name() {
		return other_name;
	}

	public void setOther_name(String other_name) {
		this.other_name = other_name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getComplexion() {
		return complexion;
	}

	public void setComplexion(String complexion) {
		this.complexion = complexion;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getKin_id() {
		return kin_id;
	}

	public void setKin_id(int kin_id) {
		this.kin_id = kin_id;
	}

	public String getKin_surname() {
		return kin_surname;
	}

	public void setKin_surname(String kin_surname) {
		this.kin_surname = kin_surname;
	}

	public String getKin_other_names() {
		return kin_other_names;
	}

	public void setKin_other_names(String kin_other_names) {
		this.kin_other_names = kin_other_names;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getKin_phone() {
		return kin_phone;
	}

	public void setKin_phone(String kin_phone) {
		this.kin_phone = kin_phone;
	}

	public String getCrime_committed() {
		return crime_committed;
	}

	public void setCrime_committed(String crime_committed) {
		this.crime_committed = crime_committed;
	}

	public String getOffence_nature() {
		return offence_nature;
	}

	public void setOffence_nature(String offence_nature) {
		this.offence_nature = offence_nature;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getSentence_start_date() {
		return sentence_start_date;
	}

	public void setSentence_start_date(String sentence_start_date) {
		this.sentence_start_date = sentence_start_date;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getCourt() {
		return court;
	}

	public void setCourt(String court) {
		this.court = court;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	//	public String getDob() {
//		return dob;
//	}
//
//	public void setDob(String dob) {
//		this.dob = dob;
//	}
//
//	public int getPrisonerId() {
//		return prisonerId;
//	}
//
//	public void setPrisonerId(int prisonerId) {
//		this.prisonerId = prisonerId;
//	}
//
//	public int getId_number() {
//		return id_number;
//	}
//
//	public void setId_number(int id_number) {
//		this.id_number = id_number;
//	}
//
//	public String getInmateUuid() {
//		return inmateUuid;
//	}
//
//	public void setInmateUuid(String inmateUuid) {
//		this.inmateUuid = inmateUuid;
//	}
//
//	public String getSurname() {
//		return surname;
//	}
//
//	public void setSurname(String surname) {
//		this.surname = surname;
//	}
//
//	public String getFirstname() {
//		return firstname;
//	}
//
//	public void setFirstname(String firstname) {
//		this.firstname = firstname;
//	}
//
//	public String getOther_name() {
//		return other_name;
//	}
//
//	public void setOther_name(String other_name) {
//		this.other_name = other_name;
//	}
//
//	public int getWeight() {
//		return weight;
//	}
//
//	public void setWeight(int weight) {
//		this.weight = weight;
//	}
//
//	public int getHeight() {
//		return height;
//	}
//
//	public void setHeight(int height) {
//		this.height = height;
//	}
//
//	public String getComplexion() {
//		return complexion;
//	}
//
//	public void setComplexion(String complexion) {
//		this.complexion = complexion;
//	}
//
////	public String getTown() {
////		return town;
////	}
////
////	public void setTown(String town) {
////		this.town = town;
////	}
//
//	public String getCounty() {
//		return county;
//	}
//
//	public void setCounty(String county) {
//		this.county = county;
//	}
//
////	public String getDistrict() {
////		return district;
////	}
////
////	public void setDistrict(String district) {
////		this.district = district;
////	}
//
//	public String getLocation() {
//		return location;
//	}
//
//	public void setLocation(String location) {
//		this.location = location;
//	}
//
//	public int getKin_id() {
//		return kin_id;
//	}
//
//	public void setKin_id(int kin_id) {
//		this.kin_id = kin_id;
//	}
//
//	public String getKin_surname() {
//		return kin_surname;
//	}
//
//	public void setKin_surname(String kin_surname) {
//		this.kin_surname = kin_surname;
//	}
//
//	public String getKin_other_names() {
//		return kin_other_names;
//	}
//
//	public void setKin_other_names(String kin_other_names) {
//		this.kin_other_names = kin_other_names;
//	}
//
//	public String getRelationship() {
//		return relationship;
//	}
//
//	public void setRelationship(String relationship) {
//		this.relationship = relationship;
//	}
//
//	public String getKin_phone() {
//		return kin_phone;
//	}
//
//	public void setKin_phone(String kin_phone) {
//		this.kin_phone = kin_phone;
//	}
//
//	public String getCrime_committed() {
//		return crime_committed;
//	}
//
//	public void setCrime_committed(String crime_committed) {
//		this.crime_committed = crime_committed;
//	}
//
//	public String getOffence_nature() {
//		return offence_nature;
//	}
//
//	public void setOffence_nature(String offence_nature) {
//		this.offence_nature = offence_nature;
//	}
//
//	public String getDuration() {
//		return duration;
//	}
//
//	public void setDuration(String duration) {
//		this.duration = duration;
//	}
//
//	public String getSentence_start_date() {
//		return sentence_start_date;
//	}
//
//	public void setSentence_start_date(String sentence_start_date) {
//		this.sentence_start_date = sentence_start_date;
//	}
//
//	public String getCourt() {
//		return court;
//	}
//
//	public void setCourt(String court) {
//		this.court = court;
//	}
//
//	public String getComments() {
//		return comments;
//	}
//
//	public void setComments(String comments) {
//		this.comments = comments;
//	}
//

	@Override public String toString() {
		return "Inmate{" +
				"prisonerId=" + prisonerId +
				", id_number=" + id_number +
				", surname='" + surname + '\'' +
				", firstname='" + firstname + '\'' +
				", other_name='" + other_name + '\'' +
				", dob='" + dob + '\'' +
				", weight=" + weight +
				", height=" + height +
				", complexion='" + complexion + '\'' +
				", county='" + county + '\'' +
				", location='" + location + '\'' +
				", kin_id=" + kin_id +
				", kin_surname='" + kin_surname + '\'' +
				", kin_other_names='" + kin_other_names + '\'' +
				", relationship='" + relationship + '\'' +
				", kin_phone='" + kin_phone + '\'' +
				", crime_committed='" + crime_committed + '\'' +
				", offence_nature='" + offence_nature + '\'' +
				", duration='" + duration + '\'' +
				", sentence_start_date='" + sentence_start_date + '\'' +
				", release_date='" + release_date + '\'' +
				", court='" + court + '\'' +
				", comments='" + comments + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}
