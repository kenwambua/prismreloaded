/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.prism_project.listener;

import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;

/**
 * Created by benjamin on 9/12/14.
 */
public class Log4jContextListener implements ServletContextListener {

	/**
	 * Initialize log4j when the application is being started
	 */
	@Override public void contextInitialized(ServletContextEvent event) {
		// initialize log4j here
		ServletContext context = event.getServletContext();
		String log4jConfigFile = context.getInitParameter("log4j-config-location");
		String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
		PropertyConfigurator.configure(fullPath);
	}

	@Override public void contextDestroyed(ServletContextEvent event) {
		// do nothing
	}
}
