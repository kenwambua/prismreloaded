# README #

Here is a documentation of how to run the project and how to get involved with the project.

### What is this repository for? ###

*

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
		junit:junit:3.8.1
		Maven: org.springframework:spring-web:4.1.2.RELEASE
		Maven: org.springframework:spring-aop:4.1.2.RELEASE
		Maven: org.springframework:spring-beans:4.1.2.RELEASE
		Maven: org.springframework:spring-context:4.1.2.RELEASE
		Maven: org.springframework:spring-core:4.1.2.RELEASE
		Maven: commons-logging:commons-logging:1.1.3
		Maven: org.springframework:spring-webmvc:4.1.2.RELEASE
		Maven: org.springframework:spring-expression:4.1.2.RELEASE
		Maven: org.springframework.security:spring-security-web:3.2.5.RELEASE
		Maven: aopalliance:aopalliance:1.0
		Maven: org.springframework.security:spring-security-core:3.2.5.RELEASE
		Maven: org.springframework.security:spring-security-config:3.2.5.RELEASE
		Maven: org.springframework:spring-jdbc:4.1.2.RELEASE
		Maven: org.springframework:spring-tx:4.1.2.RELEASE
		Maven: jstl:jstl:1.2
		Maven: mysql:mysql-connector-java:5.1.6
		Maven: org.hibernate:hibernate-entitymanager:4.3.6.Final
		Maven: org.jboss.logging:jboss-logging:3.1.3.GA
		Maven: org.jboss.logging:jboss-logging-annotations:1.2.0.Beta1
		Maven: dom4j:dom4j:1.6.1
		Maven: xml-apis:xml-apis:1.0.b2
		Maven: org.hibernate.common:hibernate-commons-annotations:4.0.5.Final
		Maven: org.hibernate.javax.persistence:hibernate-jpa-2.1-api:1.0.0.Final
		Maven: org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec:1.0.0.Final
		Maven: org.javassist:javassist:3.18.1-GA
		Maven: org.hibernate:hibernate-testing:4.3.6.Final
		Maven: org.jboss.byteman:byteman:2.1.2
		Maven: org.jboss.byteman:byteman-install:2.1.2

* Database configuration
	Sql Database
	Use Liquibase plugin to Write the database

* How to run tests

* Deployment instructions
	* System Requirements
		Core i3 4gb ram 320gb HDD
	* Software Requirement
		Tomcat 8
		Maven 3
		JDK 1.7
		Mysql Server
		Ngix Server
	* Installation Instructions
		Get the .war file
		Run it Using Tomcat



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Kennedy Wambua